GRYFUN allows the visualization, filtering and subsequent analysis of Gene Ontology (GO) annotation profiles. A GO annotation functional profile consists of the collection of GO terms that annotate a given set of proteins. (e.g. protein family). GO comprehends three orthogonal ontology aspects - biological process, molecular function and cellular component. Each of these three GO aspects are structured as Directed Acyclic Graphs (DAGs).


GRYFUN's central visualization mode revolves around generating and displaying sub-graphs that subsume all the GO annotations for a given protein set. The represented DAGs are comprised of nodes and edges and whereas each node represents a GO term and the connecting directed edges represent taxonomic relationships between them. On the original GO DAGs these would be is_a relationships where starting from children and pointing towards parents, all converging into a common root parental term. However, on GRYFUN the edge direction is reversed. Also, the edge thickness is proportional to the number of proteins annotated to any given (non-leaf node) that manage to be annotated by one of its children terms. This couple of features gives a visual cue on both term distribution and how overall the specificity of GO annotations within a given protein set. Additionally, relevant GO-based annotation and statistic metrics are also displayed. Moreover, this webtool enables the user to interactively create further subsets of proteins associated to terms by clicking their respective nodes on the DAG.


==============================================


GRYFUN web app can be installed, for example, on a server running a linux distribution running a webserver (e.g. apache2), a relational database management system (e.g. mysql) and Python >=2.6.X installed.


DOWNLOAD and INSTALL the web2py framework on your server (http://www.web2py.com/init/default/download). Refer to web2py documentation on how to setup up web2py on your particular server configuration.


Further GRYFUN dependencies to INSTALL include:
-----------------------------------------------

Graphviz package - www.graphviz.org/

PyGraphviz - pygraphviz.github.io/

NetworkX - https://networkx.github.io/

The fisher python module - https://pypi.python.org/pypi/fisher/


DOWNLOAD and UNTAR the re-processed UniProtKb (2014-03) database dumps:
-------------------------------------------------------------

http://xldb.fc.ul.pt/tools/erebus/downloads/00_gryfuns_extra_table_statements.tgz

http://xldb.fc.ul.pt/tools/erebus/downloads/01_gryfuns_extra_table_dumps.tgz

http://xldb.fc.ul.pt/tools/erebus/downloads/GOA.tgz

http://xldb.fc.ul.pt/tools/erebus/downloads/prot_acc.tgz

http://xldb.fc.ul.pt/tools/erebus/downloads/prot.tgz


MD5 Checksums:
--------------

203f62c70036cd36cd525934ec53fd3a  00_gryfuns_extra_table_statements.tgz

9beb2419fb89a490350b889477f69526  01_gryfuns_extra_table_dumps.tgz

a3fbf1807314c7d87eecf0ef9bbae6ad  GOA.tgz

967bd397b0e635d113d5426465773fdd  prot_acc.tgz

fde68884102b0d99cf58c513bf549d7e  prot.tgz



At your "mysql" installation:
=============================

- Create the database name "gryfun" (or different name if you altered the default GRYFUN app definitions)

- Import the downloaded gryfun tables ( mysql -u root -p gryfun < *.sql )

- Load each of the table dumpfiles into the database

(
    LOAD DATA LOCAL INFILE '/path/to/file/table_name.txt' INTO TABLE gryfun.table_name;

    or

    mysqlimport -u root -p gryfun table_name.txt
) 


After configuring the web2py framework on your server:
------------------------------------------------------

- Add the 'gryfun' folder inside the applications directory of your web2py instalation.

- Run the GRYFUN app once (access its respective link as per web2py configuration); after a successful run the admin/managing tables should be created.



===================================================================
BELOW are listed alterations you will need to make to gryfun files
in order to match your own personal configurations and run GRYFUN
without any further modification (beyond these changes).
===================================================================


At the mysql console execute the following commands:
-----------------------------------------------------

USE gryfun;

INSERT INTO auth_group (role,description) VALUES('guest', 'Group for read-only unregistered users to experiment');
INSERT INTO auth_group (role,description) VALUES('user', 'Group for normal users');
INSERT INTO auth_group (role,description) VALUES ('temp', 'Group for session-register only temporary users');
INSERT INTO auth_membership (user_id, group_id) VALUES (1, 3);
INSERT INTO auth_membership (user_id, group_id) VALUES (2, 4);


- This will create two group for Guests (read-only and temporary session-life users) and Regular users that match the initial GRYFUN configurations, alternatively you can edit the configuration files.


At your GRYFUN installation folder EDIT the following fields of the 
specificed files according to your settings and/or preferences:
====================================================================

@ controllers/default.py
-------------------------
EDIT...

guest_login (method)

guest_id (value, default = 1)

# MODIFY ACCORDING TO YOUR CHOSEN TEMP USER GROUP ID
tempusergrpID (value, default = 3)

temp_grp_id (value, default = 3) 

@ models/db.py
--------------
EDIT...

db (value)

mail.settings (values)


## configure auth policy
auth.settings.registration_requires_verification = True
auth.settings.registration_requires_approval = False
auth.settings.reset_password_requires_verification = True

# Puts every new user in the 'user' role (CHANGE LATER)
# According to which the actual 'user group id' will be
auth.settings.everybody_group_id = 4







