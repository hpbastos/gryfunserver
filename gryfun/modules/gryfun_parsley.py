#!/usr/bin/python26
# -*- coding: iso-8859-15 -*-
from gluon.html import A, DIV, H3, BUTTON, OPTION, TAG, I, XML, URL
from gluon.sqlhtml import SQLFORM
from gluon.compileapp import LOAD
from gluon.http import HTTP
from gluon import current

# needed the auth.user.id to
# insert in some of thetables
from gluon.tools import Auth


import os, sys, re, subprocess
from decimal import *
import fisher as f

import networkx as nx
import multitest as mtc

# DEBUG MODE ONLY
#from gluon.debug import dbg


# ============================================================================================= #
# ============================================================================================= #

class Gryfun_Grapher(object):
    #"""Class to suit (or try to suit) my graph handling needs
    #   using mostly the modules networkx and graphviz!"""
    def __init__(self, request, nodes=[], edges=[]):
    #def __init__(self, request, dag): #nodes=[], edges=[]):
        super(Gryfun_Grapher, self).__init__()

        self.dag = nx.DiGraph()  #dag

        self.nodes = nodes
        self.edges = edges

        self.request = request

        if len(self.nodes):
            self.dag.add_nodes_from(self.nodes)
        if len(self.edges):
            self.dag.add_edges_from(self.edges)


    # export/save to file a graph in dot format
    def export_dot(self, save):

        P=nx.to_pydot(self.dag)

        savename = save+'.dot'
        fpath = os.path.join(self.request.folder, 'static', 'images', 'dots', savename)

        P.write(fpath)
        return fpath


    # system call to 'dot' binary to generate .png and .map files
    def make_mapped_png(self, save):

        pdot = os.path.join(self.request.folder, 'static', 'images', 'dots', save+'.dot')
        pmap = os.path.join(self.request.folder, 'static', 'images', 'maps', save+'.map')
        ppng = os.path.join(self.request.folder, 'static', 'images', 'maps', save+'.png')

        with open(pmap,'w') as fout:
            subprocess.call(['dot', '-Tcmapx', pdot], stdout=fout)

        with open(ppng,'w') as fout:
            subprocess.call(['dot', '-Tpng', pdot], stdout=fout)


    # fetches the relevant mapping text from the original map file
    # to be returned as the area mapping info on the webpage
    def fetch_mapping_info(self, savename):

        fmap = os.path.join(self.request.folder, 'static', 'images', 'maps', savename+'.map')

        MF = open(fmap, 'r')
        tmap = MF.readlines()

        S = ''

        subGtag = re.compile('^(<area shape="rect" [^href]+? )href(=.+)')
        substitute = re.compile('^(<area shape="poly" [^href]+? )href(="initDivPos\(.+)')
        for t in tmap:
            sub = substitute.search(t)
            gtag = subGtag.search(t)
            if sub:
                S = S + sub.group(1) + 'style="cursor:pointer;" onclick' + sub.group(2)
            if gtag:
                S = S + gtag.group(1) + ' onclick' + gtag.group(2)
        return S


    # prints (sends out) the inner html code necessary to present
    # the mapped png on the webpage specified div area
    def include_png_map(self, imgname, text):

        fpng = URL('static/images/maps', imgname+'.png')

        return '<IMG id="DAGimg" style="border:none; max-width:none;" \
                NAME="'+imgname+'" SRC="'+fpng+'" USEMAP="#'+imgname+'"/> \
                <MAP id="DAGmap" NAME="'+imgname+'">'+text+'</MAP>'




# ============================================================================================= #
""" ========================================================================================= """
""" ========================================================================================= """
# ============================================================================================= #


class Gryfun_Handler(object):
    """docstring for Gryfun_Handler"""
    def __init__(self, db):
        super(Gryfun_Handler, self).__init__()

        self.db = db

        auth = Auth(db)
        self.user_id = auth.user.id


    # fetches the internal termid given a go term name
    def get_term_id_by_name(self, name):

        tid = self.db(self.db.term.name == name).select(self.db.term.id)
        return tid[0]['id']

    # fetches GO term name descriptor given the (mygo) internal term id
    def get_name_by_term_id(self, tid):

        name = self.db(self.db.term.id == tid).select(self.db.term.name)
        return name[0]['name']

    # fetches GO accession descriptor given the (mygo) internal term id
    def get_accession_by_term_id(self, tid):

        name = self.db(self.db.term.id == tid).select(self.db.term.acc)
        return name[0]['acc']

    # fetches term Accession, Name and (rel) IC for a given term 'id'
    def get_go_term_info(self, tid):

        info = self.db(self.db.term.id == tid).select(self.db.term.acc, self.db.term.name, self.db.term.ic)
        return info[0]['acc'], info[0]['name'], info[0]['ic']

    # retrieves Collection and Set names
    def get_colset_names(self, coll, fset):

        collection = self.db( self.db.collection.id == coll  ).select(self.db.collection.name, distinct=True)
        setname = self.db( self.db.protset.id == fset  ).select(self.db.protset.name, distinct=True)
        return collection[0]['name'], setname[0]['name']

    # retrieves all prots in a given collection/set
    def retrieve_total_prots(self, collection, protset):

        prots = self.db( (self.db.anthology.record_id == self.user_id) \
            & (self.db.anthology.coll_acc == collection) & (self.db.anthology.set_acc == protset)).select( \
            self.db.anthology.prot_id, distinct=True)

        protlist = [int(x.prot_id) for x in prots]

        return protlist

    # retrives protein ids from given coll/protset annotated in a given ontology
    def retrieve_collection_prots_by_ontology(self, collection, ont):

        if ont == "biological_process":
            aspect = self.db.prot_GOA_BP
        if ont == "molecular_function":
            aspect = self.db.prot_GOA_MF
        if ont == "cellular_component":
            aspect = self.db.prot_GOA_CC

        prots = self.db( (self.db.anthology.prot_id == aspect.prot_id) & \
            (self.db.anthology.record_id == self.user_id) & (self.db.anthology.coll_acc == collection) \
            ).select(self.db.anthology.prot_id, distinct=True)

        protlist = [int(x.prot_id) for x in prots]

        return protlist

    # retrieves unique proteins in collection annotated to a given term
    def retrieve_collection_prots_with_term(self, coll, tid):

        res = self.db( (self.db.term.id == tid) ).select(self.db.term.term_type)
        ont = res[0]['term_type']

        if ont == "biological_process":
            aspect = self.db.prot_GOA_BP
        if ont == "molecular_function":
            aspect = self.db.prot_GOA_MF
        if ont == "cellular_component":
            aspect = self.db.prot_GOA_CC

        prots = self.db( (self.db.anthology.prot_id == aspect.prot_id) & \
            (aspect.term_id == tid) & (self.db.anthology.record_id == self.user_id) & \
            (self.db.anthology.coll_acc == coll) \
            ).select(self.db.anthology.prot_id, distinct=True)

        protlist = [int(x.prot_id) for x in prots]

        return protlist


    # retrives protein ids from given coll/protset annotated in a given ontology
    def retrieve_set_prot_ids(self, collection, protset, ont, evidence):

        if ont == "biological_process":
            aspect = self.db.prot_GOA_BP
        if ont == "molecular_function":
            aspect = self.db.prot_GOA_MF
        if ont == "cellular_component":
            aspect = self.db.prot_GOA_CC

        prots = self.db( (self.db.anthology.prot_id == aspect.prot_id) & \
	    (aspect.evidence.belongs( evidence ) ) & \
            (self.db.anthology.record_id == self.user_id) & (self.db.anthology.coll_acc == collection) & \
            (self.db.anthology.set_acc == protset)).select(self.db.anthology.prot_id, distinct=True)

        protlist = [int(x.prot_id) for x in prots]

        return protlist

    # get direct annotation terms for a given prot list (and ontology aspect)
    def retrieve_direct_go_term_ids(self, protlist, ont, evid):

        if ont == "biological_process":
            goa = self.db.prot_GOA_BP
        if ont == "molecular_function":
            goa = self.db.prot_GOA_MF
        if ont == "cellular_component":
            goa = self.db.prot_GOA_CC


#    	evidence = [x for x in evid]
#   	evidence = tuple(evidence)

        prots = protlist

        termids = self.db( (self.db.term.id == goa.term_id) & (goa.prot_id == self.db.prot_acc.prot_id) & \
        (goa.is_redundant == 0) & (self.db.prot_acc.is_primary == 1) & (self.db.prot_acc.prot_id.belongs( prots )  ) \
        ).select(self.db.term.id, distinct=True)

        # termids is a 'term.id' Rows object...
        L = []
        for term in termids:
            L.append(int(term['id']))

        return L

    # Sets a term as root to filter out all other terms [takes list of node/terms as input]
    def root_graph_at_term(self, termlist, root):

        L = []
        for term in termlist:
            res = self.db( (self.db.graph_path.term1_id == root ) & (self.db.graph_path.term2_id == term ) & \
                (self.db.graph_path.relationship_type_id == 1) ).select(self.db.graph_path.id)
            if len(res) != 0:
                L.append(term)
        return L


    # fetches the COMPLETE term (id) path [as list of tuples] for each term (mygo internal id) in a GIVEN list
    # USEFUL for re-building GO sub-graphs (from specific nodes re-tracing all the way to the root)
    def get_multiple_term_ancestors(self, termlist, current_edges, root, rootlist):


        edgelist = []

        mother = self.get_term_id_by_name('all')

        clonelist = [x for x in termlist]
        terms = []

            gp = self.db.graph_path.with_alias('gp')

        for term in clonelist:
                if term in terms:
                continue
            else:
            termx = self.db( (gp.relationship_type_id == 1) & \
                (gp.term2_id == term)  ).select(gp.term1_id, distinct=True)

            for row in termx:
                t = int(row['term1_id'])
                if t not in terms and t != int(mother):
                    terms.append(t)

        terms = [x for x in terms if x in rootlist]


        tupl = self.db( (gp.distance == 1) & (gp.relationship_type_id == 1) & \
            (gp.term2_id.belongs(terms))  ).select(gp.term1_id, gp.term2_id, distinct=True)


        for tu in tupl:
            if int(tu['term2_id']) in rootlist and int(tu['term1_id']) in rootlist: 
            edgelist.append( ( int(tu['term1_id']), int(tu['term2_id']) ) )

        return edgelist

# ====================================================================================================


    def get_all_decendants_of_term(self, term):

        descendants = self.db( (self.db.graph_path.term1_id == term) & \
	(self.db.graph_path.relationship_type_id == 1) ).select(self.db.graph_path.term2_id, distinct=True)

	desc = [int(x['term2_id']) for x in descendants]

	return desc



    # ========================================================================================= #
    # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% #
    # ========================================================================================= #



    # Perks up the generated mapped/png DAGs so that they are more informative
    # BEWARE! CAUTION! the 'dot' mapping parser does not handle very well unexpected
    # quotation marks (" "). Inserting these on the wrong place and it will break its
    # graph/node/edge atribute parsing.
    def enhance_dot(self, filename, coll, fset, groot, ont, evidence, rel=[], color='burlywood'):

        os.rename(filename, filename+"~")

        FR = open(filename+'~', 'r')
        FW = open(filename, 'w')

        FSH = open(filename+'.bak', 'w') # .dot file to share
        
        dot = FR.readlines()
        patt = re.compile('^(\d+);$') # to capture all nodes (and not edges)


        ont = "'"+ont[0]+"'" # workaroud to deal with quotation mark issue...


        a = 0
        # for each LINE on the dot file...
        for d in dot:

            a += 1
            if a == 2:
                FW.write('graph [URL="javacript:void(0);"];\n')     # rotate="90"
                
                FSH.write('graph[];\n')

            found = patt.search(d)
            if found: # KEYLINES FOR ADDING IMPROVEMENTS GO BELOW THIS LINE
                term = str(found.group(1))
                term_int = int(found.group(1)) # to feed the tooltip stats fetcher

                truncterm = self.process_term_name(term)

                if term_int in rel:
                    tip_n = self.get_name_by_term_id(term)
                    tip_s = self.get_fam_term_repr(coll, fset, term_int, groot, evidence)      # gets term frequency

                    truncterm = self.process_term_name(tip_n)
                    
                    tip = tip_n + tip_s
                    ecoll = "'"+coll+"'"
                    efset = "'"+fset+"'"

                    # .dot file to share
                    termacc = self.get_accession_by_term_id(term)
                    FSH.write(termacc+' [fontsize="8.0" color="'+color+'" style="filled" tooltip="'+tip+'" id="'+term+'" label="'+tip_n+'" ];\n')

                                        
                    FW.write(term+' [fontsize="8.0" color="'+color+'" style="filled" tooltip="'+tip+'" id="'+term+'" label="'+truncterm+'" URL="initDivPos(event); createInfo('+ecoll+','+efset+','+term+','+ont+','+str(groot)+');"];\n')    # label="'+goid+'" initDivPos(event);

                else:
                    tip_n = self.get_name_by_term_id(term)
                    tip_s = self.get_fam_term_repr(coll, fset, term_int, groot, evidence)
                    tip = tip_n + tip_s

                    ecoll = "'"+coll+"'"
                    efset = "'"+fset+"'"

                    # .dot file to share
                    termacc = self.get_accession_by_term_id(term)
                    FSH.write(termacc+' [fontsize="12.0" tooltip="'+tip_n+'" id="'+term+'" label="..." ];\n')  
                    
                    FW.write(term+' [fontsize="12.0" tooltip="'+tip+'" id="'+term+'" label="..." URL="initDivPos(event);createInfo('+ecoll+','+efset+','+term+','+ont+','+str(groot)+');"];\n')     # label="'+goid+'"     shape="circle"
                    #FW.write(term+' [fontsize="12.0" tooltip="'+tip+'" id="'+term+'" label="..." URL="createInfo('+ecoll+','+efset+','+term+','+ont+','+str(groot)+');"];\n') initDivPos(event)

            else:
                FW.write(d+'\n')
                # .dot file to share
                FSH.write(d+'\n')
                
        FR.close()
        FW.close()
        FSH.close()        
        os.remove(filename+'~')


    # processes a 'term name' in order to obtain a truncated version to better fit a node
    def process_term_name(self, name):

        if len(name) > 20:                      # IF the complete 'term name' has MORE than 20 characters
            chars = 0
            words = 0
            trunc_name = ''
            sname = name.split(' ')

            if len(sname[0]) > 10:              # if 1st word has MORE than 10 characters
                return sname[0][0:9] + '...'    # RETURN the first 10 characters plus '...'

            else:
                for e in sname:
                    if len(e) > 10:             # if 2nd (and subsequent) word(s) have MORE than 10 characters
                        return trunc_name + e[0:9] + '...'
                        sys.exit(0);
                    else:
                        if (chars + len(e)) > 20:
                            diff = 20 - chars
                            return trunc_name + e[0:diff] + '...'
                        else:
                            trunc_name = trunc_name + e + '\\n'
                            chars = chars + len(e)
                    if chars < 20:
                        if e == sname[-1]:
                            return trunc_name
                        else:
                            continue
                    else:
                        return trunc_name   #+' ...'

        else:                                   # IF the complete 'term name' has LESS than 20 characters
            trunc_name = ''
            sname = name.split(' ')
            for e in sname:
                trunc_name = trunc_name + e + '\\n'
            return trunc_name


    # enhances the edges on a dot file
    def enhance_dot_edges(self, filename, coll, fset, groot, evidence):

        os.rename(filename, filename+"~")

        FR = open(filename+'~', 'r')
        FW = open(filename, 'w')

        # .dot file to share
        os.rename(filename+'.bak', filename+".bak~")
        RBAK = open(filename+'.bak~', 'r')
        FSH = open(filename+'.bak', 'w') 
        dotsh = RBAK.readlines()
                        
        dot = FR.readlines()
        patt = re.compile('^\d+\s->\s(\d+)')

        t_gR_entr = len(self.get_entries_with_term(coll, fset, groot, evidence))

        for d in dot:
            found = patt.search(d)
            if found: # KEYLINES FOR ADDING IMPROVEMENTS GO BELOW THIS LINE
                tid = int(found.group(1))
                t_X_entr = len(self.get_entries_with_term(coll, fset, tid, evidence))

                getcontext().prec = 3
                r_xt = Decimal(str(t_X_entr))/Decimal(str(t_gR_entr)) #*100

                if (int(t_X_entr) < 2):
                    FW.write(found.group(0)+' [style="dotted"];\n')
                else:
                    FW.write(found.group(0)+' [style="solid" penwidth="'+str(1+(r_xt*5))+'"];\n')
            else:
                FW.write(d+'\n')

        FR.close()
        FW.close()
        os.remove(filename+'~')

        # second round to enhance the downloadable dot file edges...
        patt2 = re.compile('^(\d+)\s->\s(\d+)')
                        
        for d in dotsh:
            found = patt2.search(d)
            if found: # KEYLINES FOR ADDING IMPROVEMENTS GO BELOW THIS LINE
                tid = int(found.group(2))
                t_X_entr = len(self.get_entries_with_term(coll, fset, tid, evidence))

                getcontext().prec = 3
                r_xt = Decimal(str(t_X_entr))/Decimal(str(t_gR_entr)) #*100

                if (int(t_X_entr) < 2):
                    termacc1 = self.get_accession_by_term_id( int(found.group(1)) )
                    termacc2 = self.get_accession_by_term_id( int(found.group(2)) )
                    FSH.write(termacc1+' -> '+termacc2+' [style="dotted"];\n')
                else:
                    termacc1 = self.get_accession_by_term_id( int(found.group(1)) )
                    termacc2 = self.get_accession_by_term_id( int(found.group(1)) )
                    FSH.write(termacc1+' -> '+termacc2+' [style="solid" penwidth="'+str(1+(r_xt*5))+'"];\n')
            else:
                FSH.write(d+'\n') 
                       
        RBAK.close()
        FSH.close()
        os.remove(filename+'.bak~')


   # fetches term absolute & percent representativity in a family
    def get_fam_term_repr(self, coll, fset, tid, root, evidence):

        total_entries_w_x_term = len( self.get_entries_with_term(coll, fset, tid, evidence) )
        total_entries_w_ca = len( self.get_entries_with_term(coll, fset, root, evidence) )

        getcontext().prec = 3
        trep = Decimal(str(total_entries_w_x_term))/Decimal(str(total_entries_w_ca))*100

        trepline =  ' [('+str(total_entries_w_x_term)+'/'+str(total_entries_w_ca)+') '+str(trep)+'%]'

        return trepline


    # returns 'prot_id' entries for a given 'collection' and
    # 'fset' that are annotated with a given term id ('tid')
    def get_entries_with_term(self, collection, fset, tid, evid):


        tt = self.db(self.db.term.id == tid).select(self.db.term.term_type)
        tt = tt[0]['term_type']
        if tt == 'biological_process':
            goa = self.db.prot_GOA_BP
        if tt == 'molecular_function':
            goa = self.db.prot_GOA_MF
        if tt == 'cellular_component':
            goa = self.db.prot_GOA_CC

        evidence = [x for x in evid]

        prots = self.db( (self.db.anthology.prot_id == goa.prot_id) & \
            (goa.term_id == self.db.graph_path.term2_id) & \
	    (goa.evidence.belongs(evidence) ) & \
            (self.db.graph_path.term1_id == tid) & \
            (self.db.anthology.record_id == self.user_id) & \
            (self.db.anthology.coll_acc == collection) & \
            (self.db.anthology.set_acc == fset) & \
            (self.db.graph_path.relationship_type_id == 1) ).select(self.db.anthology.prot_id, distinct=True)

        protlist = [int(x.prot_id) for x in prots]
        return protlist



    """
    ---------------------------------------------------------------------------------------------------------
    Fetches from the 'mygo' database concerning TAXONOMY info
    ---------------------------------------------------------------------------------------------------------
    """


    # fetches a left_value from the 'species' table given a UniProt 'acc'
    def get_species_left_value(self, prot):

        sp_id = self.db( (self.db.species.ncbi_taxa_id == self.db.prot.sp_id) & \
                (self.db.prot.id == prot) ).select(self.db.species.left_value)

        return str(sp_id[0]['left_value'])


    # fetches the lower and upper boundaries of a given tax rank (string)
    def get_tax_rank_bounds(self, taxrank):

        bounds = self.db(self.db.species.genus == taxrank).select(self.db.species.left_value,\
                 self.db.species.right_value)

        rank_lower = str(bounds[0]['left_value'])
        rank_upper = str(bounds[0]['right_value'])

        return rank_lower, rank_upper


    # Sort a given list of proteins into list of [given] ranks
    def sort_tax_ranks(self, prots, ranks=['Archaea', 'Bacteria', 'Eukaryota', 'Viruses']):

        rankdic = {}
        rankbounds ={}

        for r in ranks:
            rankbounds[r] = self.get_tax_rank_bounds(r)

        for p in prots:
            token = 0
            leftvalue = self.get_species_left_value(p)
            for r in ranks:
                try:
                    # Compares the lower (left) boundary a rank's boundaries
                    if (int(leftvalue) > int(rankbounds[r][0]) and int(leftvalue) < int(rankbounds[r][1])):
                        rankdic.setdefault(r, []).append(p)
                        token += 1
                except:
                    continue
            if token == 0:
                rankdic.setdefault('Other', []).append(p)

        return rankdic


    # fetches a Genus species values from the 'species' table given an (internal) prot id
    def get_genus_species_name(self, prot):

        res = self.db( (self.db.species.ncbi_taxa_id == self.db.prot.sp_id) & \
            (self.db.prot.id == self.db.prot_acc.prot_id) & \
            (self.db.prot.id == prot) ).select(self.db.species.left_value, \
            self.db.species.genus, self.db.species.species, self.db.prot_acc.acc)
        return str(res[0]['species']['left_value']), res[0]['species']['genus'], res[0]['species']['species'], res[0]['prot_acc']['acc']


    
    # calculates the term scores based on the relative IC
    def compute_term_scores(self, X, dag, coll, fset, root, ont, daterms, evidence):

        if ont == 'biological_process':
            goa = 'prot_GOA_BP'
        if ont == 'molecular_function':
            goa = 'prot_GOA_MF'
        if ont == 'cellular_component':
            goa = 'prot_GOA_CC'

        # total number of [ontology aspect] annotated prots in Collection (m) -- population
        M = len( X.retrieve_collection_prots_by_ontology(coll, ont) )
 
        # total number of proteins in Set -- set size (n) -- study set
        N = len( X.retrieve_set_prot_ids(coll, fset, ont, evidence) )

        # Reverse the DAG direction to match with is_a relationships
        Rdag = dag.reverse()

        # compute and populate graph object with ic-scores and p-values
        RDG = self.compute_values(X, Rdag, coll, fset, daterms, M, N, root, evidence)
        
        res = []
        for node in RDG.nodes():
            res.append( ( RDG.node[node]['go_acc'], RDG.node[node]['term_name'], \
                                      RDG.node[node]['occ'], RDG.node[node]['icscore'], \
                                      RDG.node[node]['pvalue']) )

        return res # returns list of tuples (acc, name, occ, score)

    
    
    # compute the pvalues and maybe... more?
    def compute_values(self, X, RDG, coll, fset, daterms, M, N, root, evidence):

        # Set desired statistical significance
        alpha = 0.05

        # DAG leaf-terms (root is the only common sink)
        leaves = [int(n) for n,d in RDG.in_degree().items() if d == 0]

        
        ascendants = [x for x in RDG.nodes() if x not in leaves]

        ### Processing all (directly annotated) terms & append them to nodes in graph
        ##############################################################################

        
        # for EACH node (term) on the graph...
        for da in RDG.nodes():
            
            prots = X.get_entries_with_term(coll, fset, da, evidence)

            # get term's ACC, NAME & relative_IC
            tacc, tname, tr_info = X.get_go_term_info(da)

            RDG.node[da]['go_acc'] = tacc
            RDG.node[da]['term_name'] = tname
            RDG.node[da]['rel_ic'] = tr_info
            
            # (nt) -- number of prots annotated to term 't' in study set [N]
            nt = len(prots)
            RDG.node[da]['occ'] = nt
            RDG.node[da]['len'] = nt
            
            RDG.node[da]['icscore'] = Decimal(str(tr_info))*nt/Decimal(N)  # maybe float() it later
            RDG.node[da]['prots'] = prots

            # (mt) -- number of prots annotated with term 't' in population [M]
            mt = len(X.retrieve_collection_prots_with_term(coll, da))
            RDG.node[da]['mt'] = mt
                        
            # Fisher's Exact Test p-value
            pvalue = f.pvalue(nt, N-nt, mt-nt, (M-N)-(mt-nt)).right_tail
            RDG.node[da]['pvalue'] = pvalue


        if len(ascendants) > 0:

            X.node_walking_Elim(X, leaves, ascendants, daterms, alpha, coll, fset, N, M, root, RDG, evidence)


        return RDG


    # Implementation of the Elim algorithm to adjust pvalues for ancestor nodes
    def node_walking_Elim(self, X, nodes, ascendants, directannot, alpha, coll, fset, N, M, root, RDG, evidence):

        # list of leaf-nodes to start upward paths from...
        nodelist = [x for x in nodes]
        # "dynamic visitation list"
        clonelist = [x for x in nodes]

        # para cada nó folha...
        for leaf in nodelist:
            # if leaf node already processed (due to being a sibling) skip to the next leaf
            if leaf not in clonelist:
                continue
            # proteinas que vao ter de ser removidas
            rem_prots = []

            # todos os caminhos de um nó (folha) até à raiz
            paths = self.get_all_dag_paths(RDG, leaf, root, memo_dict = None)
            for path in paths:
                # para CADA nó no CAMINHO
                for node in path:
                    # ...se for encontrado um nó DA nos ascendentes...
                    if node in ascendants:
                        try:
                            bol = RDG.node[node]['visited']
                            if bol == True:
                                continue
                        except:
                            pass
                        offspring = self.get_all_descendants_in_RDAG(RDG, node)
                        # ...vamos voltar para baixo e ver "TODOS" os seus descendentes
                        #print "Os descendentes do termo", node, "são", offspring
                        for off in offspring:
                            # if offspring is a DA descendant... (--ignoring inherited terms)
                            
                            #if off in directannot:
                            if 1 == 1:    
                                if RDG.in_degree(off) == 0 and RDG.node[off]['pvalue'] < alpha:
                                    rem_prots = rem_prots + RDG.node[off]['prots']

                                elif RDG.node[off]['pvalue'] < alpha:
                                    rem_prots = rem_prots + RDG.node[off]['prots']


                        # marked prots to remove from ancestor node (and population)
                        # and thus make adjusted p-value calculations for ancestors
                        rem_prots = set(rem_prots)
                        rem_prots = list(rem_prots)

                        # get Nt and Mt protlists for term (node)
                        ori_N_t = X.get_entries_with_term(coll, fset, node, evidence)
                        RDG.node[node]['occ'] = len(ori_N_t)
                        ori_M_t = X.retrieve_collection_prots_with_term(coll, node)
                        # remove (significant?) prots in descendants from ascendants/population list
                        nt_linha = [val for val in ori_N_t if val not in rem_prots]      ### len(nt_linha) == n_t'
                        mt_linha = [val for val in ori_M_t if val not in rem_prots]      ### len(mt_linha) == m_t'

                        nt_ = len(nt_linha)
                        mt_ = len(mt_linha)
                        N_ = N - len(rem_prots)
                        M_ = M - len(rem_prots)
                        # calculate the elim p-value
                        pv = f.pvalue(nt_, N_-nt_, mt_-nt_, (M_-N_)-(mt_-nt_)).right_tail

                        # populate node with attributes
                        tacc, tname, tr_info = self.get_go_term_info(node)

                        RDG.node[node]['go_acc'] = tacc #'"'+str(tacc)+'"'
                        RDG.node[node]['term_name'] = tname
                        RDG.node[node]['rel_ic'] = tr_info

                        RDG.node[node]['prots'] = nt_linha
                        RDG.node[node]['len'] = nt_
                        RDG.node[node]['mt'] = mt_

                        RDG.node[node]['pvalue'] = pv
                        RDG.node[node]['visited'] = True


    ### GET all path from target SOURCE to target SINK
    ### INPUT: nx DiGraph, source, sink (both int id), memoized frozenset
    ### OUTPUT all paths as tuples in frozenset
    ### CHANGE iterator line in code according to graph FLOW DIRECTION
    ### IF DAG; source 'leaf'-y node, sink 'root'-y, then  G.successors(source_node)
    ### gives us parents of 'source_node'. (graph flowing from leaves to root)
    def get_all_dag_paths(self, G, source_node, sink_node, memo_dict = None):
        if memo_dict is None:
            # putting {}, or any other mutable object
            # as the default argument is wrong
            memo_dict = dict()

        if source_node == sink_node: # Don't memoize trivial case
            return frozenset([(source_node,)])
        else:
            pair = (source_node, sink_node)
            if pair in memo_dict.keys(): # Is answer memoized already?
                return memo_dict[pair]
            else:
                result = set()
                for new_source in G.successors(source_node):   # OR for new_source in G.predecessors(source_node):
                    paths = self.get_all_dag_paths(G, new_source, sink_node, memo_dict)
                    for path in paths:
                        path = (source_node,) + path
                        result.add(path)
                result = frozenset(result)
                # Memoize answer
                memo_dict[(source_node, sink_node)] = result
                return result


    ### Returns all descendant nodes on a given Reversed DAG (like GO in networkx)
    ### INPUT: target node, networkx reversed DAG
    ### OUTPUT: list with descendant nodes of node 'X' in RDAG 'W'
    def get_all_descendants_in_RDAG(self, RDG, node, offspring = None):
        if offspring is None:
            offspring = list()

        for k in RDG.predecessors(node):

            if RDG.in_degree(k) == 0:
                offspring.append(k)
                continue
            if RDG.in_degree(k) == 1:
                offspring.append(k)
                self.get_all_descendants_in_RDAG(RDG, k, offspring)

            if RDG.in_degree(k) > 1:
                offspring.append(k)
                self.get_all_descendants_in_RDAG(RDG, k, offspring)

        return offspring



    # ===================================================================================================== #
    """
    ---------------------------------------------------------------------------------------------------------
    "Draws" the info to display associated with the DAGs
    ---------------------------------------------------------------------------------------------------------
    """
    # ===================================================================================================== #



class Gryfun_Spider(object):
    """docstring for Gryfun_Spider"""
    def __init__(self, db):
        super(Gryfun_Spider, self).__init__()

        self.db = db

        auth = Auth(db)
        self.user_id = auth.user.id

    # 'draws' up the handle of the Floater Div Window
    # ================================================
    def draw_handle(self, coll, fset, tid, root, evidence):

        X = Gryfun_Handler(self.db)

        term_name = X.get_name_by_term_id(tid)
        term_acc = X.get_accession_by_term_id(tid)

        total_entries_w_x_term = len( X.get_entries_with_term(coll, fset, tid, evidence) )
        total_entries_w_ca = len( X.get_entries_with_term(coll, fset, root, evidence) )

        getcontext().prec = 3
        trep = Decimal(str(total_entries_w_x_term))/Decimal(str(total_entries_w_ca))*100

        ### QUOTATION GALORE SYSTEM / SENDING THE POP-UP DIV AWAY ###
        divId = 'term'+str(tid)+'Div'
        divIdquote = "'"+divId+"'"
        floatersquote = "'Floaters'"
        contentdiv = '#content_'+str(tid)
        ##############################################################

        minimize = '<a title="minimize/maximize" href="javascript:void(0);" class="rightside" \
                    style="text-decoration:none;" onClick="jQuery(\''+contentdiv+'\').slideToggle(); \
                    return false;"><i class="fa fa-chevron-up"></i></a><br>'

        close = '<a title="close" style="text-decoration:none;" href="javascript:void(0);" \
                 onClick="document.getElementById('+floatersquote+').removeChild(document.getElementById('+divIdquote+'))"> \
                 <i class="fa fa-times"></i> </a>'

        handle = '<div class="leftside" style="width:86%; padding:0.2em;"> \
          <a class="handle" style="text-decoration:none;" title="'+term_name+'">'+term_acc+'</a> \
          <font color="black">('+str(total_entries_w_x_term)+'/'+str(total_entries_w_ca)+') \
          <span style="visibility:hidden;">_</span>'+str(trep)+'%</font></div> \
          <div style="width:5%; padding-top:0.2em; padding-bottom:0.2em; float:left;">'+minimize+'</div> \
          <div class="rightside" style="width:5%; padding-top:0.2em; padding-bottom:0.2em; \
          padding-right:0.2em;">'+close+'</div> <br style="clear:both;">'

        return handle

    ### ========================================================================== ###

    # 'draws' up the content of the Floater Div Window
    # =========================================================
    def draw_floater_content(self, coll, fset, tid, root, ont, evidence):

        coll_s = "'"+coll+"'"
        fset_s = "'"+fset+"'"
        ont_s ="'"+ont+"'"

        evidstring = ','.join(evidence)
        evidstring = "'"+evidstring+"'"

        X = Gryfun_Handler(self.db)

        #dbg.set_trace()
        
        prots = X.get_entries_with_term(coll, fset, tid, evidence) # current gets repeats?

        superking = X.sort_tax_ranks(prots)

        n = len(prots)

        term_name = X.get_name_by_term_id(tid)
        term_acc = X.get_accession_by_term_id(tid)

        go_id_lnk = '<a href="http://amigo.geneontology.org/cgi-bin/amigo/term_details?term='+term_acc+'" target="_blank" class="infoLinks">'+term_name+'</a>'


        rerooter = '<a title="generates new Annotation Graph using current GO term as root" style="text-decoration:none;" href="javascript:void(0);"\
                    onClick="loadDAGproper('+coll+','+fset+','+ont_s+','+str(tid)+','+evidstring+');">\
                    <span class="fa-stack fa-lg"><i class="fa fa-square-o fa-stack-2x"></i> <i class="fa fa-sitemap fa-stack-1x"></i> </span></a>'


        if 1 == 1:

            text = '<div id=inner_'+str(tid)+'> \
                    \
                    <div id=pophead_'+str(tid)+' class="setInfoDiv" style="white-space:wrap; margin-left:0.3em; \
                      border:2px solid grey; padding:0.2em; margin-bottom:4px; margin-top:4px;"> \
                      <div class="leftside" style="width:85%;"><b>term name:</b><br/>'+go_id_lnk+'</div> \
                      <div class="rightside" style="width:10%; text-align:right;">'+rerooter+'</div> \
                      <br style="clear:both;"> \
                    </div>\
                    <form id="f_'+str(tid)+'" name="f_'+str(tid)+'" method="post" action="" onsubmit="return false">'


            if n > 30:
                text2 = '<div style="width:98%; margin-left:0.15em; margin-top:0.2em; margin-bottom:0.2em;"> \
                    <input type="button" title="Download selected entries on TSV format" \
                    onClick="saveTSV('+coll_s+','+fset_s+','+ont_s+','+str(tid)+',\'prots\');" \
                    value="Export" class="contentButton" /></div>' 
                text = str(text) + str(text2)

            # manual sorting of current taxonomic groupings
            taxlist = ['Archaea', 'Bacteria', 'Eukaryota', 'Viruses', 'Other']

            for sk in taxlist:
                try:
                    if superking[sk]:
                        skfield = "#"+str(tid)+sk+"_field"

                        headerking = '<div style="border:none;"> \
                                      <div class="leftside" style="margin-left:0.25em;"> \
                            <input type="checkbox" name="'+sk+'All" value="all" \
                            onClick="checkUncheckAll(this, '+sk+')"> \
                            <a href="javascript:void(0);" onclick=""><b>'+sk+'</b></a> \
                            </input> ('+str(len(superking[sk]))+')</div> \
                            <a title="collapse/expand" href="javascript:void(0);" class="rightside" \
                            style="text-decoration:none; margin-right:0.3em;" \
                            onClick="jQuery(\''+skfield+'\').slideToggle(); return false;">\
                            <i class="fa fa-chevron-up"></i></a><br> \
                            <br style="clear:both;"></div> \
                            <div style="border:none; line-height=5px;"> \
                            \
                            <fieldset id="'+str(tid)+sk+'_field" class="fieldSet setInfoDiv" \
                            style="display:table; width:100%; margin-bottom:0.2em;"> \
                            <table id="table_'+str(tid)+'_'+sk+'" class="sortable" style="width:100%; font-size:12px"> \
                            <thead><tr> \
                            <th style="width:1em;"></th> \
                            <th style="display:none; margin-left:0.5em;">tax_left</th> \
                            <th title="Sort taxonomically" \
                            style="text-align:left; cursor:pointer; width:4em; padding-left:0.5em;" \
                            onclick="Table.sort(this,{col:1, sorttype:Sort[\'numeric\']});">\
                            <div class="leftside">Acc</div> \
                            <div class="rightside"><i class="fa fa-sort"></i></div> \
                            <div style="clear:both;"></div> \
                            </th> \
                            <th title="Sort alphabetically" style="text-align:left; cursor:pointer; \
                            padding-left:0.4em;" onclick="Table.sort(this,this);">\
                            <div class="leftside">Species</div> \
                            <div class="rightside"><i class="fa fa-sort"></i></div> \
                            <div style="clear:both;"></div> \
                            </th> \
                            </tr></thead><tbody>'
                        text = str(text) + str(headerking)

                        for p in superking[sk]:
                            l, g, s, acc = X.get_genus_species_name(p)
                            spname = str(g)+' '+str(s)
                            row =  '<tr> \
                                    <td><input type="checkbox" name="'+sk+'" value="'+acc+'" \
                                    style="padding-left:3em;" class="prot"></input></td> \
                                    <td style="display:none;">'+str(l)+'</td> \
                                    <td style="width:4em; padding-left:0.3em; padding-right:0.3em; \
                                    text-align:center;"><a href="http://www.uniprot.org/uniprot/'+acc+'" \
                                    target="_blank" class="infoLinks">'+acc+'</a></td> \
                                    <td style="padding-left:0.4em; font-style:italic;">'+spname+'</td> \
                                    </tr>'
                            text = str(text) + str(row)
                        footerking = '</tbody></table></fieldset></div>'                        
                        text = str(text) + str(footerking)

                except KeyError:
                    pass

            footer = '<div style="width:98%; margin-left:0.15em; margin-top:0.2em; margin-bottom:0.2em;"> \
                    <input type="button" title="Download selected entries on TSV format" \
                    onClick="saveTSV('+coll_s+','+fset_s+','+ont_s+','+str(tid)+',\'prots\');" \
                    value="Export" class="contentButton" /></div>'
            text = str(text) + str(footer) + '</form></div>'
            
            
            return text



    ### ################################################################################ ####



    # summons up the set related information to populate the graph header
    # ===================================================================
    def draw_set_info(self, eXtracterObj, coll, fset, root, ont, evidence):

        X = eXtracterObj

        # root of the current ontology aspect
        maxroot = int(X.get_term_id_by_name(ont))

        rootname = X.get_name_by_term_id(root)

        total_entries = len(X.retrieve_total_prots(coll, fset))

        collection, protset =  X.get_colset_names(coll, fset)

        subsetprots = X.get_entries_with_term(coll, fset, root, evidence)
        total_entries_w_x_term = len( subsetprots )

        # ??? alternative for rerooting
        ontprots = X.get_entries_with_term(coll, fset, maxroot, evidence)
        total_entries_w_maxroot = len( ontprots )

        getcontext().prec = 3
        # ontology coverage
        ontCov = Decimal(str(total_entries_w_maxroot))/Decimal(str(total_entries))*100

        # frequency by ontology
        cac = Decimal(str(total_entries_w_x_term))/Decimal(str(total_entries_w_maxroot))*100
        cacline = str(cac)+"% ("+str(total_entries_w_x_term)+")"

        # Processing  "colset" coherence scores should go here  #
        # ##################################################### #
        #
        # ##################################################### #


        left = '<div class="leftside" style="text-align:left; max-width:70%; line-height:100%;"> \
                <p style="padding-top:0.45em; padding-left:0.45em;"> \
                <b>Collection</b>: '+str(collection)+'</p> \
                <p style="padding-left:0.45em;"><b>Protein set</b>: '+str(protset)+'</p> \
                <br/> \
                <p style="padding-left:0.45em;"><b>Total set size: \
                </b>'+str(total_entries)+' protein entries; </p> \
                <p style="padding-left:0.45em; padding-bottom:0.5em;">\
                <b>Annotation coverage</b> <i title="entries in protein set annotated with terms from selected GO aspect" \
                class="fa fa-info-circle"></i>: '+str(ontCov)+'% \
                (<b>'+str(total_entries_w_maxroot)+'</b>); </p> \
                <p style="padding-left:0.45em;"><b>Current root </b><i class="fa fa-info-circle" \
                title="root term of the current annotation graph"></i>: '+rootname+'</p> \
                <p style="padding-left:0.45em;"><b>Current coverage </b><i class="fa fa-info-circle" \
                title="GO aspect coverage for current root term"></i>: '+cacline+'</p> \
                </div>'

        dagfile = '/gryfun/static/images/maps/'+str(coll)+'_'+str(fset)+'_'+ont[0]+'_r'+str(root)+'.png'
#        dotfile = '/gryfun/static/images/dots/'+str(coll)+'_'+str(fset)+'_'+ont[0]+'_r'+str(root)+'.dot.bak'
        dotfile = '/gryfun/static/images/dots/'+str(coll)+'_'+str(fset)+'_'+ont[0]+'_r'+str(root)+'.dot'

        right = '<div class="rightside" style="text-align:right; width:30%; padding-right:0.45em;"><p> \
               <div class="rightside"> \
               <a href="'+dagfile+'"  target="_blank"> \
               <button class="graphCtrlButtons btn" type="button">Export .png</button></a> \
               <a href="'+dotfile+'" target="_blank"><button class="graphCtrlButtons btn" type="button">Export .dot</button> </a> \
               </p>'

        #filename = str(coll)+'_'+str(fset)+'_'+ont[0]+'_r'+str(root)

        imgtag =  "'"+str(coll)+"_"+str(fset)+"_'"


        right2 = '<p style="padding-top:0.2em;" > \
                </div> \
                </div> <br style="clear:both;">'



        ranks = X.sort_tax_ranks(subsetprots)

        # MANUAL SORTING of CURRENT taxonomic groupings
        taxlist = ['Archaea', 'Bacteria', 'Eukaryota', 'Viruses', 'Other']

        footer = '<p style="padding-top:0.35em;padding-left:0.45em;padding-bottom:0.45em;p">'

        for r in taxlist:
            try:
                if ranks[r]:
                    footer = footer + '<b>'+r+':</b> ('+str(len(ranks[r]))+') '
            except KeyError:
                pass

        footer = footer + '</p>'

        return left+right+right2+footer


    # summons up the set related information to populate the graph header
    # ===================================================================
    def draw_term_stats(self, X, dag, coll, fset, root, ont, daterms, evidence):

        terms = X.compute_term_scores(X, dag, coll, fset, root, ont, daterms, evidence)

        header = '<div> \
            <form id="t_'+str(root)+'" name="t_'+str(root)+'" method="post" action="" onsubmit="return false">'

        if terms:
            header2 = '<div style="width:98%; margin:0.25em;"> \
                <input class="btn" type="button" title="Exports the selected terms information into a TSV file" \
                onClick="saveTSV(\''+str(coll)+'\',\''+str(fset)+'\',\''+ont+'\','+str(root)+',\'terms\');" \
                value="Export terms"  /> \
                </div> \
                <div style="border:none;"> \
                <table id="terms_'+str(coll)+'_'+str(fset)+'" class="sortable" style="width:100%; \
                margin-left:auto; margin-right:auto;"> \
                <thead><tr> \
                <th style="width:0.7em;"> \
                <input type="checkbox" title="Select all GO terms" name="checkall" \
                value="all" onClick="checkUncheckAllToo(this)"></input> \
                <th style="display:none;"></th> \
                \
                <th style="text-align:left; cursor:pointer; padding-left:0.4em;" class="sort-text" > \
                <div class="leftside"> term names <i class="fa fa-info-circle" title="GO term names"></i></div>  \
                <div class="rightside"><i class="fa fa-sort"></i></div> \
                <div style="clear:both;"></div> \
                </th> \
                <th style="text-align:left; cursor:pointer; width:4em; padding-left:0.5em;" class="sort-number"> \
                <div class="leftside">occ <i class="fa fa-info-circle" title="# annotations"></i></div> \
                <div class="rightside"><i class="fa fa-sort"></i></div> \
                <div style="clear:both;"></div> \
                </th> \
                <th style="text-align:left; cursor:pointer;" class="sort-number"> \
                <div class="leftside"> IC score <i class="fa fa-info-circle" title="term representativity IC-based score"></i> </div> \
                <div class="rightside"><i class="fa fa-sort"></i></div> \
                <div style="clear:both;"></div> \
                </th> \
                <th style="text-align:left; cursor:pointer;" class="sort-number sort-default"> \
                <div class="leftside">p-value <i class="fa fa-info-circle" title="nominal p-value using Collection as background" ></i> </div> \
                <div class="rightside"><i class="fa fa-sort"></i></div> \
                <div style="clear:both;"></div> \
                </th> \
                \
                \
                \
                \
                </tr></thead> \
                <tbody>'


            text = header + header2

            for t in terms:
                score = "%.3f" % round(float(t[3]), 3)
                pvalue = "%.3e" % t[4]

                tablerow = '<tr> \
                   <td><input type="checkbox" name="goterm" value="'+t[0]+'" class="term"></input></td> \
                   <td style="display:none;">'+t[0]+'</td> \
                   <td style="padding-left:0.4em;">'+t[1]+'</td> \
                   <td style="padding-right:0.4em; text-align:right;">'+str(t[2])+'</td> \
                   <td style="padding-right:0.4em; text-align:right;">'+str(score)+'</td> \
                   <td style="padding-right:0.4em; text-align:right;">'+str(pvalue)+'</td> \
                   \
                   </tr>'
                text = text + tablerow

            footer = '</tbody></table></div>'
            text = text + footer

        return text + '</form>'
