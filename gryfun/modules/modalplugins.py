#-*- encoding:utf-8 -*- # * formModal
from gluon.html import A, DIV, H3, BUTTON, OPTION, TAG, I, XML
from gluon.sqlhtml import SQLFORM
from gluon.compileapp import LOAD
from gluon.http import HTTP
from gluon import current

# needed the auth.user.id to
# insert in some of thetables
from gluon.tools import Auth
import cgi


# added the additional db param (DAL object) & parent (cascading menus)
class Modal(object):
    def __init__(self, field, value, title_btn, title_modal, db):
        self.field = field
        self.value = value
        self.title_btn = title_btn
        self.title_modal = title_modal
        
        self.key = str(self.field).replace('.', '_')
        self.modal_id = 'modal_%s' % self.key
        self._target = "c_" + self.key
        
        self.request = current.request
        self.response = current.response
        self.session = current.session

        self.db = db
        
    
    def btn_show(self):
        # Button to trigger modal
        btn_show_modal = A(I(_class="icon-plus-sign"),
                             ' ', self.value,
                             **{"_role": "button",
                             "_class": "btn btn-link",
                             "_data-toggle": "modal",
                             "_href": "#%s" % self.modal_id,
                             "_title": self.title_btn,
                             # adding to the party...
                             "_id": "button_%s" % self.field}
                             )
                             
        return btn_show_modal

    ### The actual "looks" of the modal form window
    def div_modal(self, content_modal):

        div_modal = DIV(
                         DIV(
                             H3(self.title_modal, _id="myModalLabel"),
                             _class="modal-header"),
                             
                         DIV(content_modal, _class="modal-body"),
                         
                         DIV(
                             BUTTON("Close", **{"_class": "btn",
                                                "_data-dismiss": "modal",
                                                "_aria-hidden": "true"}),
                                                
                                                _class="modal-footer",
                             ),
                         **{"_id": "%s" % self.modal_id,
                            "_class": "modal hide face",
                            "_tabindex": "-1",
                            "_role": "dialog",
                            "_aria-hidden": "true",
                            "_aria-labelledby": "myModalLabel"}
                        )
                        
                        
                        
        return div_modal


    # ----------------------------------------------------------------------- #
    # ----------------------------------------------------------------------- #
        
    def create(self):

        if not self.field.type.startswith('reference'):
            raise SyntaxError("Can only be used with field reference")
        if not hasattr(self.field.requires, 'options'):
            raise SyntaxError("Can not determine options")
         
            
                  
        if self.request.get_vars._ajax_add == str(self.field):
            # this gets the columns on target reference table eg. ['collection'/'protset']
            table = self.field._db[self.field.type[10:]]
            raise HTTP(200, self.checkForm(table))
            
        return self.btn_show()

    
    ### Modal 'final/trigger?' LOADer (form generator) function
    def formModal(self):
        
        return self.div_modal(LOAD(self.request.controller,
                                     self.request.function,
                                     args=self.request.args,
                                     vars=dict(_ajax_add=self.field),
                                     target=self._target,
                                     ajax=True)
                              )
        


    
    def checkForm(self, table):
        
        
        auth = Auth(self.db) # maybe temp ()
        
        
        formnamemodal = "formmodal_%s" % self.key      
        form = SQLFORM(table, formname=formnamemodal)

        if form.accepts(self.request.vars,
                        self.session,
                        formname=formnamemodal, onvalidation=self.sanitize_input ):

            options = TAG[''](*[OPTION(v,
                                       _value=k,
                                       _selected=str(form.vars.id) == str(k))
                               
                             for (k, v) in self.field.requires.options()]
                             )
            _cmd = "jQuery('#%s').html('%s');"
            _cmd += "jQuery('#%s').modal('hide');"

           # VERY App Specific HACKs ...
           # ======================================================================
              
            # if requesting COLLECTION modal form...
            if(self.field.name == 'coll_acc') and ('record_id' in table):
            
                _cmd += "populateInputCollections('#anthology_coll_acc');"
                _cmd += "ajax('show_user_collections', ['%s'], 'protset_collection_id');"

                command = _cmd % (self.key,
                               options.xml().replace("'", "\'"),
                               self.modal_id, auth.user.id
                              )

                self.response.flash = 'New collection created' 
            # if otherwise requesting PROTEIN SET modal form...                  
            elif (self.field.name == 'set_acc') and ('collection_id' in table):
            
                _cmd += "populateInputSets('#anthology_coll_acc');" 
               
		command = _cmd % (self.key, '', self.modal_id)
 
		
                self.response.flash = 'New protein set created' 
            # STANDARD/GENERIC/"UNADULTERED" situation                  
            else:          
                command = _cmd % (self.key,
                               options.xml().replace("'", "\'"),
                               self.modal_id
                              )    
                
                self.response.flash = 'New register added' 

                                                                                          
            self.response.js = command   
        elif form.errors:
            self.response.flash = "Your form has errors!"

        return form

    # sanitize user input in order to prevent user injected code shenanigans...
    def sanitize_input(self, form):

        base = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~ '
        form.vars.name = filter(lambda x: x in base, form.vars.name) 
	form.vars.name = cgi.escape(form.vars.name).encode('ascii', 'xmlcharrefreplace')



############################################################################################################
