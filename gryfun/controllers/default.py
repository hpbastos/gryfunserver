### -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

#########################################################################
## This is a sample controller
## - index is the default action of any application
## - user is required for authentication and authorization
## - download is for downloading files uploaded in the db (does streaming)
## - call exposes all registered services (none by default)
#########################################################################

import re, sys
from modalplugins import Modal

import networkx as nx

from gryfun_parsley import Gryfun_Handler as G2H
from gryfun_parsley import Gryfun_Grapher as G2G
from gryfun_parsley import Gryfun_Spider as G2S


def index():
    """
    example action using the internationalization operator T and flash
    rendered by views/default/index.html or views/generic.html

    if you need a simple wiki simple replace the two lines below with:
    return auth.wiki()
    """

    return dict()


def about():
    """
    Handles GRYFUN generic info
    """
    return dict()

def concepts():
    """
    Helps people on how to use GRYFUN
    """
    return dict()

def instructions():
    """
    Teaches people the flow of the GRYFUN
    """
    return dict()


def guest_login():
    """
    Logins in guest account
    """
    # EDIT
    user = auth.login_bare('CHOOSE_YOUR_GUEST_ID@USER.ACCOUNT', 'CHOOSE_YOUR_GUEST_PASSWORD')
    redirect(URL('user/profile'))


def logout_and_register():
    """
    Logs out of guest account and open register form
    """

    if auth.user:
        session.auth = None

    redirect( URL('gryfun', 'default', 'user', args=['register'], vars=dict(_next='/gryfun/default/index') ) )


def temp_user():
    """
    Creates a temporary (session) user account
    """

    tempusergrpID = 3 # MODIFY ACCORDING TO YOUR CHOSEN TEMP USER GROUP ID

    if auth.user == None:
        unique_key = response.session_id.split(':')
        
        email = str(unique_key[0])+'@gry.fun'
    
        iduser = new_user('Temporary','GRYFUNer',email,'SOMEPASSWORD', tempusergrpID)

        user = auth.login_bare(email, 'SOMEPASSWORD')
        
        redirect( URL('gryfun', 'default', 'input' ) )



def new_user(first_name, last_name, email, passw, grp_id):
    
    users = db(db.auth_user.email==email).select()
    if users:
        return users[0].id
    else:
        my_crypt = CRYPT(key=auth.settings.hmac_key)
        crypt_pass = my_crypt(passw)[0]        
        id_user= db.auth_user.insert(
                                     first_name=first_name,
                                     last_name=last_name,
                                     email = email,
                                     password = crypt_pass                             
                                     )

        db.auth_membership.insert(user_id=id_user, group_id=grp_id)

        return id_user




# ================================================================================= #
# ==================================== INPUT ====================================== #

@auth.requires_login()
@auth.requires( auth.has_membership(group_id='user') or auth.has_membership(group_id='temp') )
def input():
    """ 
      Receives and handles submission of UniProt accession
      numbers and respective population into Collections and
      Protein Sets.
    """
    # Modal to add 'New Collections'
    fieldC = db.anthology.coll_acc
    modalC = Modal(fieldC, 'New', 'New collection name', 'New Collection...', db)
    db.anthology.coll_acc.comment = modalC.create()

    # Modal to add 'New Protein Sets'
    fieldS = db.anthology.set_acc
    modalS = Modal(fieldS, 'New', 'New protein set name', 'New Protein Set...', db)
    db.anthology.set_acc.comment = modalS.create()

    formmodalC=modalC.formModal()
    formmodalS=modalS.formModal()

    # seems like that I can only create the form
    # after creating the desired modal forms (maybe)

    form = SQLFORM.factory()

    form[0].insert(-1, TR(SELECT(_id='anthology_coll_acc',
                              _name='coll_acc',
                               )) )

    form[0].insert(-1, TR(SELECT(_id='anthology_set_acc',
                              _name='set_acc', 
                              )) )

    # Extra (non-db) arbitrary/customized input fields #
    # ================================================ #
    form[0].insert(-1, TR(LABEL(T('Proteins:')),
                             TEXTAREA(_id='no_table_prots',
                                      _class='text',
                                      _name='prots',
                                      _rows='10',
                                      _cols='10'
                          )))



    form.elements('.w2p_fl', replace=None)

    # first validates -- input: possible UniProt acc? then insert matched input
    if form.accepts(request.vars, session, onvalidation=validate_protlist, dbio=False):

         try:
             for m in session.dbprots:
                 db.anthology.insert(coll_acc=form.vars.coll_acc, set_acc=form.vars.set_acc, record_id=auth.user.id, protein=m[0], prot_id=m[1])

             response.flash = "%s UniProt accession number(s) added to your set" %(len(session.dbprots))
             session.dbprots = None
         except:
             response.flash = "No UniProt acession numbers detected!"

    elif form.errors:
         #response.flash = "Something is wrong with your submission!"
         pass

    return dict(form=form, formmodalC=formmodalC, formmodalS=formmodalS)


# ============================================================ #
@auth.requires_login()
def validate_protlist(form):
    """
    Validates submitted data checking for format, and then for
    existence in the background current version mygo database.
    """

    if form.vars.coll_acc != '' and form.vars.set_acc != '':

        cset = db( (db.anthology.coll_acc == form.vars.coll_acc) & (db.anthology.set_acc == form.vars.set_acc)  ).select(db.anthology.prot_id)
        stored = [int(row['prot_id']) for row in cset]

        patt = re.compile(r'(\b\w{6}\b)', re.MULTILINE)
        matches = re.findall(patt, form.vars.prots)
        if len(matches) > 0:

            dbprots = []
            for m in matches:
                rows = db(db.prot_acc.acc == m, db.prot_acc.is_primary).select(db.prot_acc.prot_id)
                if len(rows) != 0:
                    # filter out proteins already in set (for counting sake)
                    if int(rows[0].prot_id) not in stored:
                        dbprots.append((m, rows[0].prot_id))

            session.dbprots = dbprots       # check later if saving in session is the best way
            #response.flash = "%s UniProt accession number(s) added to your set" %(len(dbprots))
        else:
            form.errors.prots = T('No UniProt acession numbers detected!')

    else:
        if form.vars.coll_acc == '' and form.vars.set_acc == '':
            response.flash = "Please select a Collection and Set!"
            form.errors.coll_acc = ''
            form.errors.set_acc = ''
        elif form.vars.set_acc == '':
            response.flash = "Please select a protein Set!"
            form.errors.set_acc = ''
     


# ================================================================================= #
# ==================================== MANAGE ===================================== #

@auth.requires_login()
@auth.requires_membership('user')
def manage():
    """
    Manages the protein collections for each user database
    (Currently Collection/Set creation only)
    TO DO: Implement Collection/Set removal
    """

    # Modal to add 'New Collections'
    fieldC = db.anthology.coll_acc
    modalC = Modal(fieldC, 'New', 'New collection name', 'New Collection...', db)
    db.anthology.coll_acc.comment = modalC.create()

    # Modal to add 'New Protein Sets'
    fieldS = db.anthology.set_acc
    modalS = Modal(fieldS, 'New', 'New protein set name', 'New Protein Set...', db)
    db.anthology.set_acc.comment = modalS.create()

    formmodalC=modalC.formModal()
    formmodalS=modalS.formModal()

    # seems like that I can only create the form
    # after creating the desired modal forms

    form = SQLFORM.factory()

    form[0].insert(-1, TR(SELECT(_id='anthology_coll_acc',
                              _name='coll_acc')) )

    form[0].insert(-1, TR(SELECT(_id='anthology_set_acc',
                              _name='set_acc')) )


    if form.accepts(request.vars, session):

         response.flash = "Registry entry created"
    elif form.errors:
         response.flash = "Something is wrong with your submission!"


    return dict(form=form, formmodalC=formmodalC, formmodalS=formmodalS)


# ================================================================================= #
# =================================== EXPLORE ===================================== #

@auth.requires_login()
def explore():
    """
    Explores the DAGs for the protein sets in each collection for each user database
    """


    MY_OPTIONS = ['EXP', 'IDA', 'IPI', 'IMP', 'IGI', 'IEP', 'ISS', 'ISO', 'ISA', 'ISM', 'IGC', 'IBA', 'IBD', 'IKR', 'IRD', 'RCA', 'NAS', 'TAS', 'IC', 'ND', 'IEA']


    form = SQLFORM.factory(Field('coll_acc',
                           requires=IS_IN_SET([])
                          ),
                          Field('set_acc',
                           requires=IS_IN_SET([])
                          ),
                          Field('aspect',
                           requires=IS_IN_SET(['biological_process', 'molecular_function', 'cellular_component'])
                          ),



			  Field("my_options", "list:string",
			    default=MY_OPTIONS,widget=SQLFORM.widgets.checkboxes.widget,
        		    requires=[IS_IN_SET(MY_OPTIONS,
			    multiple=True),IS_NOT_EMPTY()]), 




                           submit_button = 'GO!'
    )

    # first validates -- input: all fields non-blank? then...
    if request.vars.set_acc != '' and form.accepts(request.vars, session, dbio=False, keepvalues=True):

         setinfo, dag, termstats = displayDAG()

         response.setinfo = setinfo
         response.dag = dag
         response.termstats = termstats


    elif form.errors or request.vars.set_acc == '':
        form.errors.clear()
        response.flash = "ERROR: Please select a protein Collection, Set, GO Aspect and at least one annotation evidence code!"


    return dict(form=form)



# method handling the explore.html dynamic content generation
@auth.requires_login()
def displayDAG():
    
    # Gryfun_Handler (because it handles DB content) Object
    X = G2H(db)
    rerooting = False
    
    if request.vars.newroot != None:
        coll = request.vars.coll
        fset = request.vars.fset
        ont = request.vars.aspect
        root = request.vars.newroot

	evid = request.vars.evidence
	try:
	    evidence = evid.split(',')
	except:
	    evidence = []
	    evidence.append(evid)



        prots = X.get_entries_with_term(coll, fset, root, evidence)
        rerooting = True
    else:
        coll= request.vars.coll_acc
        fset = request.vars.set_acc
        ont = request.vars.aspect
        
	evid = request.vars.my_options

	if type(evid).__name__ == 'str':
	    evidence = []
	    evidence.append(evid)
	else:
	    evidence = evid



        root = X.get_term_id_by_name(ont)
        prots = X.retrieve_set_prot_ids(coll, fset, ont, evidence)

    # if set has NO proteins
    if len(prots) == 0:
        setinfo = ''
        dagpost = '<h2>NO proteins entries found on the currently selected set.</h2>'
        termstats = ''
        return setinfo, dagpost, termstats    
    
    # directly annotated terms
    daterms = X.retrieve_direct_go_term_ids(prots, ont, evidence)
    
    # root/re-roots the graph at a specified term
    r_daterms = X.root_graph_at_term(daterms, root)


    # builds the annotation digraph
    edges_ini = []
    rootdesc = X.get_all_decendants_of_term(root)
    tree = X.get_multiple_term_ancestors(r_daterms, edges_ini, root, rootdesc)



    DG = G2G(request, edges=tree)

    # Save basic dot file to disk
    filename = str(coll)+'_'+str(fset)+'_'+ont[0]+'_r'+str(root)
    dotpath = DG.export_dot(filename)

    
    X.enhance_dot(dotpath, coll, fset, root, ont, evidence, r_daterms)
    X.enhance_dot_edges(dotpath, coll, fset, root, evidence)

    DG.make_mapped_png(filename)
    pngmap = DG.fetch_mapping_info(filename)

    # Graph content DIV
    dagpost = DG.include_png_map(filename, pngmap)

    # ====================================================================================

    # Gryfun_Spider (because it weaves content) Object
    W = G2S(db)
    # Set header info... #9ac1c9 style="background-color:#9ac1c9"
    setinfo = W.draw_set_info(X, coll, fset, root, ont, evidence)
    setinfo = '<div class ="setInfoDiv" style="background-color:#9ac1c9">'+setinfo+'</div>'

        
    # if there ARE annotations to build a 'tree'...
    if len(tree) != 0:
        termstats = W.draw_term_stats(X, DG.dag, coll, fset, root, ont, r_daterms, evidence)
    else:
        termstats = ''
        dagpost = '<h2>No annotations available to build annotation graph.</h2>'

    # if performing a RE-ROOTING operation...    
    if rerooting:
        setinfo = '<div class ="setInfoDiv" style="background-color:#9ac1c9">'+setinfo+'</div>'
        text = '<div id="setInfoDiv" style="width:100%;">'+setinfo+ '</div> \
           <div id="DAGport" style="position: relative; margin: 0 auto; width:100%; \
           min-width:800px; border:1px solid orange; text-align:center; overflow:hidden;">' + dagpost + '</div> \
           <div id="termStats" style="width:100%;">' + termstats + '</div>'
        return text
    else:    
        return setinfo, dagpost, termstats





#############################################################################################################
# Methods for displaying info on webapp floaters
#############################################################################################################

# retrieves floater handle innerHTML
@auth.requires_login()
def get_handle():

    evidence = request.vars.evidence.split(',')

    W = G2S(db)
    handle_content = W.draw_handle(request.vars.coll, request.vars.fset, request.vars.tid, request.vars.root, evidence)
    return handle_content

# retrieves floater content innerHTML
@auth.requires_login()
def get_floater_content():

    evidence = request.vars.evidence.split(',')

    W = G2S(db)
    floater_content = W.draw_floater_content(request.vars.coll, request.vars.fset, request.vars.tid, request.vars.root, request.vars.ont, evidence)
    return floater_content



##############################
##     Download methods     ##
##############################

@auth.requires_login()
def download_graph():

    dbg.set_trace()
    
    return request.vars
    filename = request.vars.save_as
    png = request.vars.location

    response.headers['Content-Type'] = 'image/png'
    response.headers['Content-Disposition'] = \
                "attachment; filename=%s" % filename
    res = response.stream(open(png, "rb"))
    
    return res

@auth.requires_login()
def download_tsv():

    filename = request.vars.save_as
    tsv = request.vars.content

    response.headers['Content-Type'] = 'text/tsv'
    response.headers['Content-Disposition'] = \
                "attachment; filename=%s" % filename

    return tsv


#############################################################################################################
# Methods for deleting Collections & Sets
#############################################################################################################

@auth.requires_login()
def delete_set():

    rows = db( (db.collection.record_id == auth.user.id) & (db.collection.id == db.protset.collection_id)  ).select(db.protset.id)
    sets = [int(r.id) for r in rows]

    if int(request.vars.set_acc) in sets:

        try:
            db( ( db.anthology.record_id == auth.user.id ) & ( db.anthology.set_acc == request.vars.set_acc )  ).delete()
        except:
            pass
        try:
            db( (db.protset.id == request.vars.set_acc) ).delete()
        except:
            pass


@auth.requires_login()
def delete_coll():

    rows = db( (db.collection.record_id == auth.user.id) ).select(db.collection.id)
    colls = [int(r.id) for r in rows]

    if int(request.vars.coll_acc) in colls:

        try:
            db( ( db.anthology.record_id == auth.user.id ) & ( db.anthology.coll_acc == request.vars.coll_acc )  ).delete()
        except:
            pass
        try:
            db( ( db.protset.collection_id == request.vars.coll_acc )  ).delete()
        except:
            pass         
        try:
            db( (db.collection.id == request.vars.coll_acc) ).delete()
        except:
            pass

#############################################################################################################
# Accessory methods 
#############################################################################################################

@auth.requires_login()
def show_collection_sets():

    # generates distinct list of set_acc(s) for a given
    # collection belonging to the the current app user

    setlist = fetch_set_list()

    # sets up string to replace innerHTML
    # of the form's select element
    sub = '<option value="%s">%s</option>'

    string = '<option value=""></option>'
    for s in setlist:
        option = sub % (s[0], s[1])
        string += option

    return string

@auth.requires_login()
def show_user_collections():

    thelist  = fetch_collection_list()

    sub = '<option value="%s">%s</option>'

    string = '<option value=""></option>'
    for s in thelist:
        option = sub % (s[0], s[1])
        string += option

    return string

############################################################################################################

@auth.requires_login()
def fetch_collection_list():

    if auth.user.id > 0:
        rows = db( db.collection.record_id == auth.user.id ).select(db.collection.id, db.collection.name)
        collections = [(r.id, r.name) for r in rows]

        return collections
    else:
        return []

@auth.requires_login()
def fetch_set_list():

    if request.vars.coll_acc > 0:
        rows = db( db.protset.collection_id == request.vars.coll_acc ).select(db.protset.id, db.protset.name, distinct=True)
        sets = [(r.id, r.name) for r in rows]

        return sets
    else:
        return []

############################################################################################################

def register():
    return dict(form=auth.register())

def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    """

    # EDIT
    # change value according to which id is attributed to the Guest account
    guest_id = 1
    temp_grp_id = 3 # MODIFY ACCORDING TO YOUR CHOSEN TEMP USER GROUP ID

    if auth.user:
        try:
            res = db( db.auth_membership.user_id == auth.user.id  ).select(db.auth_membership.group_id)
            grp_id = int(res[0]['group_id'])
        except:
            pass

    # --------- Control 'temp users' account access control ---------- #

        if 'profile' in request.args and grp_id == temp_grp_id:
	    return dict(temp=True, guest=False)	

        if 'change_password' in request.args and grp_id == temp_grp_id:
	    return dict(temp=True, guest=False)	

    # -----------------------------------------------------------------


        if 'profile' in request.args and auth.user.id == guest_id:
	    return dict(guest=True, temp=False)	

        if 'change_password' in request.args and auth.user.id == guest_id:
            return dict(guest=True, temp=False)

    # -----------------------------------------------------------------


    return dict(form=auth(), guest=False, temp=False)


@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()


@auth.requires_signature()
def data():
    """
    http://..../[app]/default/data/tables
    http://..../[app]/default/data/create/[table]
    http://..../[app]/default/data/read/[table]/[id]
    http://..../[app]/default/data/update/[table]/[id]
    http://..../[app]/default/data/delete/[table]/[id]
    http://..../[app]/default/data/select/[table]
    http://..../[app]/default/data/search/[table]
    but URLs must be signed, i.e. linked with
      A('table',_href=URL('data/tables',user_signature=True))
    or with the signed load operator
      LOAD('default','data.load',args='tables',ajax=True,user_signature=True)
    """
    return dict(form=crud())
