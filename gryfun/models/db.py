# -*- coding: utf-8 -*-

#########################################################################
## This scaffolding model makes your app work on Google App Engine too
## File is released under public domain and you can use without limitations
#########################################################################


db = DAL('mysql://DBUSERNAME:PASSWORD@localhost/gryfun', db_codec='latin1')

#dbtemp  = DAL('sqlite://storage.sqlite',pool_size=1,check_reserved=['all'])

## if SSL/HTTPS is properly configured and you want all HTTP requests to
## be redirected to HTTPS, uncomment the line below:
# request.requires_https()

###if not request.env.web2py_runtime_gae:
    ## if NOT running on Google App Engine use SQLite or other DB
###    db = DAL('sqlite://storage.sqlite',pool_size=1,check_reserved=['all'])
###else:
    ## connect to Google BigTable (optional 'google:datastore://namespace')
###    db = DAL('google:datastore')
    ## store sessions and tickets there
###    session.connect(request, response, db=db)
    ## or store session in Memcache, Redis, etc.
    ## from gluon.contrib.memdb import MEMDB
    ## from google.appengine.api.memcache import Client
    ## session.connect(request, response, db = MEMDB(Client()))

## by default give a view/generic.extension to all actions from localhost
## none otherwise. a pattern can be 'controller/function.extension'
response.generic_patterns = ['*'] if request.is_local else []
## (optional) optimize handling of static files
# response.optimize_css = 'concat,minify,inline'
# response.optimize_js = 'concat,minify,inline'

#########################################################################
## Here is sample code if you need for
## - email capabilities
## - authentication (registration, login, logout, ... )
## - authorization (role based authorization)
## - services (xml, csv, json, xmlrpc, jsonrpc, amf, rss)
## - old style crud actions
## (more options discussed in gluon/tools.py)
#########################################################################

from gluon.tools import Auth, Crud, Service, PluginManager, prettydate
auth = Auth(db)
crud, service, plugins = Crud(db), Service(), PluginManager()

## create all tables needed by auth if not custom tables
auth.define_tables(username=False, signature=False)



auth.messages.verify_email_subject = 'GRYFUN email verification'

auth.messages.verify_email = '''Thank you for registering an account with GRYFUN - GRaph analYzer of FUNctional annotation. \n
Please click on the following link %(link)s to verify your email and activate your account. \n
(Alternatively copy and paste the link above unto a new web browser page.)\n
\n\n
P.S. - Do NOT reply to this email since it is not monitored.'''

auth.messages.reset_password_subject = 'GRYFUN password reset'
auth.messages.reset_password = '''A GRYFUN password reset request has been recieved for this account.\n
To reset your password, please follow this link %(link)s \n
If you have not requested this action you can disregard this email. \n
\n
Thank you
\n\n
P.S. - Do NOT reply to this email since it is not monitored.'''


## configure email
mail = auth.settings.mailer
#mail.settings.server = 'smtp.gmail.com:587'
#mail.settings.server = 'logging' or 'smtp.gmail.com:587'
#mail.settings.sender = 'you@gmail.com'
#mail.settings.login = 'username:password'

mail.settings.tls = True
mail.settings.server = 'smtps.example.mail.server.com:465'

mail.settings.sender = 'user@example.mail.sender.com'
mail.settings.login = 'username:password'






## configure auth policy
auth.settings.registration_requires_verification = True
auth.settings.registration_requires_approval = False
auth.settings.reset_password_requires_verification = True

# Pus every new user in the 'user' role (CHANGE LATER)
# According to which the actual 'user group id' will be
auth.settings.everybody_group_id = 4


# remove afterwards
# auth.define_tables(migrate=False)

## if you need to use OpenID, Facebook, MySpace, Twitter, Linkedin, etc.
## register with janrain.com, write your domain:api_key in private/janrain.key
from gluon.contrib.login_methods.rpx_account import use_janrain
use_janrain(auth, filename='private/janrain.key')

#########################################################################
## Define your tables below (or better in another model file) for example
##
## >>> db.define_table('mytable',Field('myfield','string'))
##
## Fields can be 'string','text','password','integer','double','boolean'
##       'date','time','datetime','blob','upload', 'reference TABLENAME'
## There is an implicit 'id integer autoincrement' field
## Consult manual for more options, validators, etc.
##
## More API examples for controllers:
##
## >>> db.mytable.insert(myfield='value')
## >>> rows=db(db.mytable.myfield=='value').select(db.mytable.ALL)
## >>> for row in rows: print row.id, row.myfield
#########################################################################

## after defining tables, uncomment below to enable auditing
# auth.enable_record_versioning(db)


#########################################################################
### GRYFUN ADDITIONS
#########################################################################

### TABLE mapping 'collection' names & owners
### ===================================================================================================================
db.define_table('collection',
                 Field('name'),
                 Field('record_id', 'integer', writable=False,readable=False),
                 Field('combo', unique=True, compute=lambda r: str(r.record_id) + '_' + r.name),
                 format='%(name)s'
               )

db.collection.record_id.requires = IS_NOT_IN_DB( db( db.collection.combo == str(request.vars.record_id)+'_'+str(request.vars.name) ), 'collection.record_id')

db.collection.name.requires = IS_NOT_IN_DB( db(db.collection.combo == str(request.vars.record_id) + '_' + str(request.vars.name) ), 'collection.name')

if auth.user:
    db.collection.record_id.default = auth.user.id


### TABLE mapping 'protein sets (protset)' names
### & respective 'collection' name affiliation
### ========================================================================================
db.define_table('protset',
                 Field('name'),
                 Field('collection_id', db.collection),
                 Field('combo', unique=True, compute=lambda r: str(r.collection_id) + '_' + r.name),
                 format='%(name)s'
               )

db.protset.collection_id.requires = IS_NOT_IN_DB( db( db.protset.combo == str(request.vars.collection_id)+'_'+str(request.vars.name) ), 'protset.collection_id')

db.protset.name.requires = IS_NOT_IN_DB( db(db.protset.combo == str(request.vars.collection_id)+ '_' + str(request.vars.name) ), 'protset.name')


if auth.user:
    query = db( db.collection.record_id == auth.user.id )
    db.protset.collection_id.requires = IS_IN_DB( query, db.collection.id, '%(name)s' )



### ====================================================================================================================

db.define_table('anthology',
                    Field('coll_acc', db.collection),
                    Field('set_acc', db.protset),
                    Field('record_id', 'integer', writable=False,readable=False),
                    Field('protein', 'string', length='255', writable=False,readable=False),
                    Field('prot_id', 'integer', writable=False,readable=False),
                    Field('combo', unique=True, compute=lambda r: str(r.coll_acc) + '_' + str(r.set_acc) + '_' + str(r.record_id) + '_' +str(r.protein))
                    )



### MyGO db tables
### ====================================================================================================================

db.define_table('prot_acc',
                    Field('prot_id', 'integer'),
                    Field('acc', 'string'),
                    Field('is_primary', 'integer'),
                    Field('is_ambiguous', 'integer'),
                    migrate=False
                    )

db.define_table('graph_path',
                    Field('term1_id', 'integer'),
                    Field('term2_id', 'integer'),
                    Field('relationship_type_id', 'integer'),
                    Field('distance', 'integer'),
                    Field('relation_distance', 'integer'),
                    Field('n_paths', 'integer'),
                    migrate=False
                    )

db.define_table('prot',
                    Field('dataset', 'string'),
                    Field('name', 'string'),
                    Field('sp_id', 'integer'),
                    Field('seq', 'text'),
                    migrate=False
                 )  

db.define_table('prot_GOA_BP',
                    Field('prot_id', 'integer'),
                    Field('term_id', 'integer'),
                    Field('evidence', 'string'),
                    Field('is_redundant', 'integer'),
                    migrate=False
                 )

db.define_table('prot_GOA_MF',
                    Field('prot_id', 'integer'),
                    Field('term_id', 'integer'),
                    Field('evidence', 'string'),
                    Field('is_redundant', 'integer'),
                    migrate=False
                 )

db.define_table('prot_GOA_CC',
                    Field('prot_id', 'integer'),
                    Field('term_id', 'integer'),
                    Field('evidence', 'string'),
                    Field('is_redundant', 'integer'),
                    migrate=False
                 )

db.define_table('term',
                    Field('name', 'string'),
                    Field('term_type', 'string'),
                    Field('acc', 'string'),
                    Field('is_obsolete', 'integer'),
                    Field('is_root', 'integer'),
                    Field('is_relation', 'integer'),
                    Field('annot', 'integer'),
                    Field('prob'),
                    Field('ic'),
                    Field('annot_ne', 'integer'),
                    Field('prob_ne'),
                    Field('ic_ne'),
                    Field('descend', 'integer'),
                    Field('ic_descend'),
                    migrate=False
                 )

db.define_table('species',
                    Field('ncbi_taxa_id', 'integer'),
                    Field('common_name', 'string'),
                    Field('lineage_string', 'text'),
                    Field('genus', 'string'),
                    Field('species', 'string'),
                    Field('parent_id', 'integer'),
                    Field('left_value', 'integer'),
                    Field('right_value', 'integer'),
                    Field('taxonomic_rank', 'string'),
                    migrate=False
                 )
