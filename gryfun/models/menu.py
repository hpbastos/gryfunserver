## -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

#########################################################################
## Customize your APP title, subtitle and menus here
#########################################################################

response.logo = A( XML('GRYFUN <sub>beta</sub>'), _class="brand", _href=URL('default', 'index') )

response.title = 'GRYFUN'


## read more at http://dev.w3.org/html5/markup/meta.name.html
response.meta.author = 'hbastos <hbastos@xldb.di.fc.ul.pt>'
response.meta.description = 'GRYFUN - GRaph analYzer of FUNctional annotation'
response.meta.keywords = 'Gene ontology, protein annotation, visualization, web2py, python, framework'
response.meta.generator = 'Functional annotation analysis tool resorting to graph visualizations'

## your http://google.com/analytics id
response.google_analytics_id = None

#########################################################################
## this is the main application menu add/remove items as required
#########################################################################

response.menu = [

    (T('Menu'), False, URL('', ''), [



    (T('Add protein sets'), False, URL('default', 'input') ),
    
    (T('Explore protein sets'), False, URL('default', 'explore') ),
    
    (T('Manage collections'), False, URL('default', 'manage') ),


    ]), 

    
    (T('Help'), False, URL('', ''), [

    (T('Instructions'), False, URL('default', 'instructions') ),
    (T('Concepts'), False, URL('default', 'concepts') ),
    (T('About'), False, URL('default', 'about') ),

    ])

]

DEVELOPMENT_MENU = False

if "auth" in locals(): auth.wikimenu()
