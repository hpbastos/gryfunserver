
// populate Select Set Dropdown elements
function populateSets(elemId, col, last){

  jQuery.ajax({
  url: "show_collection_sets",
  data: { coll_acc: col },
  type: "POST",
  dataType: "html",
  success: function(data) {
    jQuery(elemId).html(data);
  },
  error: function (xhr, status) {
   alert("Sorry, there was a problem!");
  },
  complete: function (xhr, status) {
    if (last !== "None"){
       jQuery(elemId+' option').filter(function(){
         return this.value == last;
     }).prop('selected', true); }
    else{
      try{ jQuery("a#delete_anthology_set_acc").remove(); }catch(err){}; }
  }

  });
}

// populate Select Collection Dropdown elements
function populateCollections(elemId, lastC, lastS){

  jQuery.ajax({

  url: "show_user_collections",
  type: "POST",
  dataType: "html",
  success: function(data) {
    jQuery(elemId).html(data);
  },
  error: function (xhr, status) {
   alert("Sorry, there was a problem!");
  },
  complete: function (xhr, status) {
    if (lastC !== "None"){
        jQuery(elemId+' option').filter(function(){
            return this.value == lastC;
        }).prop('selected', true);

        populateSets('#anthology_set_acc', lastC, lastS); }
  }

  });
}

// ============================================================================= //

// auto-selects a Newly created collection (& adds +New link for new sets)
function populateInputCollections(elemId){

  jQuery.ajax({
    url: "show_user_collections",
    type: "POST",
    dataType: "html",
    success: function(data) {
      jQuery(elemId).html(data);
    },
    complete: function (xhr, status) {
      jQuery('#anthology_coll_acc option').last().prop('selected',true);

      var plus = '<a id="button_anthology.set_acc" title="New protein set name" role="button" href="#modal_anthology_set_acc" \
                data-toggle="modal"><i class="icon-plus-sign"></i>New</a>';
      var zeroSet = '<select id="anthology_set_acc" class="generic-widget" name="set_acc" > \
                    <option value=""></option></select>';
      jQuery('#anthology_set_acc__row').html(zeroSet);
      if( !(jQuery('a#button_anthology_set_acc').length) ){ jQuery(plus).insertAfter('#anthology_set_acc'); }

      if(typeof manage != 'undefined'){
      if(manage){

        var collection_id = jQuery('#anthology_coll_acc').val();

        var colMinus = '<a title="Delete Collection" role="button" id="delete_anthology_coll_acc" href="#null" \
                        onclick="if(confirm(\'Are you sure you want to delete this collection and all of its protein sets?\')){\
                        ajax(\'/gryfun/default/delete_coll?coll_acc='+collection_id+'\',[],\'\');jQuery(this).closest(\'tr\').remove();\
                        init_collection_protset_cascade_manage(uid); };\
                        var e = arguments[0] || window.event; e.cancelBubble=true; if (e.stopPropagation) {\
                        e.stopPropagation(); e.stopImmediatePropagation(); e.preventDefault();}">\
                        <i class="icon-minus-sign"></i>Delete </a>';

        try{ jQuery("a#delete_anthology_coll_acc").remove(); }catch(err){}; // just a precaution?


        if( !(jQuery('a#delete_anthology_coll_acc').length) ){ jQuery(colMinus).insertAfter('#anthology_coll_acc'); }
      }

      } // CLOSING THE OUTER 'manage'

    }

  }); // closes .ajax()
}     // closes function


// auto-selects Collection & Set after adding a +New set 
function autoSelectInputCollectionSet(elemId, col_id, set_id){

  jQuery.ajax({

    url: "show_collection_sets",
    data: {coll_acc: col_id},
    type: "POST",
    dataType: "html",
    success: function(data) {
      jQuery('#anthology_set_acc').html(data);
    },

    complete: function (xhr, status) {

      jQuery(elemId+' option').filter(function(){
        return this.value == col_id;
      }).prop("selected", true);

      jQuery('#anthology_set_acc option').last().prop('selected', true);

      if(typeof manage != 'undefined'){
      if(manage){

        var set = jQuery('#anthology_set_acc').val();

        var setMinus = '<a title="Delete Set" role="button" id="delete_anthology_set_acc" href="#null" \
                        onclick="if(confirm(\'Are you sure you want to delete this protein set?\')){\
                        ajax(\'/gryfun/default/delete_set?set_acc='+set+'\',[],\'\');jQuery(this).closest(\'tr\').remove();\
                        populateSets(\'#anthology_set_acc\', '+col_id+', \'None\')  };\
                        var e = arguments[0] || window.event; e.cancelBubble=true; if (e.stopPropagation) {\
                        e.stopPropagation(); e.stopImmediatePropagation(); e.preventDefault();}">\
                        <i class="icon-minus-sign"></i>Delete </a>';

        try{ jQuery("a#delete_anthology_set_acc").remove(); }catch(err){}; // just a precaution?

        if( !(jQuery('a#delete_anthology_set_acc').length) ){ jQuery(setMinus).insertAfter('#anthology_set_acc'); }
      }

      } // closing the outer 'manage'

    }

  });
}



// populates Sets on input page after a New set add
function populateInputSets(elemId){

  var col_id = jQuery('#anthology_coll_acc').val();

  jQuery.ajax({

    url: "show_user_collections",
    type: "POST",
    dataType: "html",
    success: function(data) {
      jQuery(elemId).html(data);
    },

    complete: function (xhr, status) {
      var set_id = jQuery('#anthology_set_acc option').last().val();
      autoSelectInputCollectionSet(elemId, col_id, set_id);
    }

  });
}






// ============================================================================================== //
// ===================================== INPUT PAGE HANDLING ==================================== //
// ============================================================================================== //

function init_collection_protset_cascade(uid) {

    // 'reset' protset dropdown list
    jQuery('#anthology_set_acc').html('<option value=""></option>');

    // Add 'New Collection' button/link after the Collections' dropdown list
    var plusCol = '<a id="button_anthology.coll_acc" title="New Collection name" role="button" href="#modal_anthology_coll_acc" \
                       data-toggle="modal"><i class="icon-plus-sign"></i>New</a>';
    if( !(jQuery('a#button_anthology\\.coll_acc').length) ){ jQuery(plusCol).insertAfter('#anthology_coll_acc'); }

    // Set protein set select dropdown to 'zero-ed mode'
    var zeroSet = '<select id="anthology_set_acc" class="generic-widget" name="set_acc" > \
                    <option value="">Please select a collection</option></select>';
    jQuery('#anthology_set_acc__row').html(zeroSet);

    // 'New Protein Set...' button/link
    var plus = '<a id="button_anthology.set_acc" title="New protein set name" role="button" href="#modal_anthology_set_acc" \
                    data-toggle="modal"><i class="icon-plus-sign"></i>New</a>';

    // retrieves & displays each user's own collections   
    ajax('show_user_collections', [uid], 'anthology_coll_acc');


    
    // shows corresponding sets whenever different collections are SELECTED
    // Monitors CHANGE of the COLLECTION drop-down selection list
    jQuery('#anthology_coll_acc').change(function(){
        
        var collection_id = jQuery('#anthology_coll_acc').val();
        // If I choose a collection from the dropdown menu...
        if(collection_id !== ""){

            jQuery('#anthology_set_acc__row').html(zeroSet);
            ajax('show_collection_sets', ['coll_acc'], 'anthology_set_acc');

            if( !(jQuery('a#button_anthology_set_acc').length) ){ jQuery(plus).insertAfter('#anthology_set_acc'); }
                                    
        }
        // 'resets' protset dropdown to 'zero'
        else{
            jQuery('#anthology_set_acc__row').html(zeroSet);
            jQuery('a#button_anthology.set_acc').detach();
        }
    });    // close collection change monitoring

}


// Replaces the "protset dropdown selection list" (ON the MODAL window) 
// with the "single value" (coll_acc) captured from the Collection list
function replace_protset_dropdown() {

    jQuery("#anthology_set_acc__row").delegate("a", "click", function(){
                
        var collection_id = jQuery('#anthology_coll_acc').val();

        if(collection_id !== ""){
           
            var setrow = '<tr id="protset_collection_id__row" style="display: none"> <td class="w2p_fw">\
                          <input id="protset_collection_id" name="collection_id" value="'+ collection_id +'" \
                          class="string" type="hidden" ></td> <td class="w2p_fc"></td> </tr>';

            jQuery("#protset_collection_id__row").remove();

            jQuery(setrow).insertBefore('#c_anthology_set_acc > form > table > tbody > #submit_record__row');

        }        
    });

}


// ============================================================================================== //
// ==================================== MANAGE PAGE HANDLING ==================================== //
// ============================================================================================== //


function init_collection_protset_cascade_manage(uid) {


    // Add 'New Collection' button/link after the Collections' dropdown list
    var colPlus = '<a id="button_anthology.coll_acc" title="New Collection name" role="button" href="#modal_anthology_coll_acc" \
                          data-toggle="modal"><i class="icon-plus-sign"></i>New</a>';
    if( !(jQuery('a#button_anthology\\.coll_acc').length) ){ jQuery(colPlus).insertAfter('#anthology_coll_acc'); }
                                    

    // 'resets' SET dropdown list (-- to be used when NO Collection is select)
    var selectset = '<select id="anthology_set_acc" class="generic-widget" name="set_acc" > \
                              <option value="">Please select a collection</option></select>';
    jQuery("#anthology_set_acc__row").html(selectset);
    

    try{ jQuery("a#delete_anthology_coll_acc").remove(); }catch(err){}; // just a precaution?


    var setPlus = '<a id="button_anthology.set_acc" title="New protein set name" role="button" \
                       href="#modal_anthology_set_acc" data-toggle="modal"><i class="icon-plus-sign"></i>New </a>';

 
    // retrieves & displays user collections   
    ajax('show_user_collections', [uid], 'anthology_coll_acc');
    
    // shows corresponding sets whenever different collections are SELECTED
    // it monitors CHANGES on the COLLECTION drop-down selection list
    jQuery('#anthology_coll_acc').change(function(){
        
        var collection_id = jQuery('#anthology_coll_acc').val();

        // If I choose a collection from the dropdown menu...
        if(collection_id !== ""){

            jQuery('#anthology_set_acc__row').html(selectset);               // 'resets' Set list
            ajax('show_collection_sets', ['coll_acc'], 'anthology_set_acc'); // gets and populates sets from collection


            // Adds "Delete" for Collection if there isn't one already
            var colMinus = '<a title="Delete Collection" role="button" id="delete_anthology_coll_acc" href="#null" \
                        onclick="if(confirm(\'Are you sure you want to delete this collection and all of its protein sets?\')){\
                        ajax(\'/gryfun/default/delete_coll?coll_acc='+collection_id+'\',[],\'\');jQuery(this).closest(\'tr\').remove();\
                        init_collection_protset_cascade_manage(uid); };\
                        var e = arguments[0] || window.event; e.cancelBubble=true; if (e.stopPropagation) {\
                        e.stopPropagation(); e.stopImmediatePropagation(); e.preventDefault();}">\
                        <i class="icon-minus-sign"></i>Delete </a>';

            try{ jQuery("a#delete_anthology_coll_acc").remove(); }catch(err){}; // just a precaution?



            if( !(jQuery('a#delete_anthology_coll_acc').length) ){ jQuery(colMinus).insertAfter('#anthology_coll_acc'); }

            // Adds "New" for Set if there isn't one already
            if( !(jQuery('a#button_anthology\\.set_acc').length) ){ jQuery(setPlus).insertAfter('#anthology_set_acc'); }

                                    
        }
        // NO Collection is selected --> 'resets' protset dropdown to 'zero'
        else{
            jQuery('#anthology_set_acc__row').html(selectset);  // 'resets' Set list
            jQuery("a#delete_anthology_coll_acc").remove();     // remove Delete (collection) button
        }
        
    });

}


// controls the add/removal of Delete button/link on the manage page
function init_protset_list_manage(){

    // It monitors changes on the chosen value of the PROT(ein) SET list
    jQuery("#anthology_set_acc__row").change(function(){

    var set = jQuery('#anthology_set_acc').val();
    if( set !== "" ){

      try{ var col_id = jQuery('#anthology_coll_acc').val(); } catch(err){ var col_id = 0;  } 

        var setMinus = '<a title="Delete Set" role="button" id="delete_anthology_set_acc" href="#null" \
                        onclick="if(confirm(\'Are you sure you want to delete this protein set?\')){\
                        ajax(\'/gryfun/default/delete_set?set_acc='+set+'\',[],\'\');jQuery(this).closest(\'tr\').remove();\
                        populateSets(\'#anthology_set_acc\', '+col_id+', \'None\')  };\
                        var e = arguments[0] || window.event; e.cancelBubble=true; if (e.stopPropagation) {\
                        e.stopPropagation(); e.stopImmediatePropagation(); e.preventDefault();}">\
                        <i class="icon-minus-sign"></i>Delete </a>';

        try{ jQuery("a#delete_anthology_set_acc").remove(); }catch(err){}; // just a precaution?
 
        if( !(jQuery("a#delete_anthology_set_acc").length) ) { jQuery(setMinus).insertAfter("#anthology_set_acc"); }

    }
    else{ jQuery("a#delete_anthology_set_acc").remove(); }

    });

}


// ============================================================================================== //
// ==================================== EXPLORE PAGE HANDLING =================================== //
// ============================================================================================== //

function init_collection_protset_cascade_explore(uid, lastC, lastS) {

    // 'resets' set dropdown list (-- to be used when NO Collection is select)
    var selectset = '<select id="anthology_set_acc" class="generic-widget" name="set_acc" > \
                     <option value="">Please select a collection</option></select>';
    jQuery("#anthology_set_acc__row").html(selectset);
    
    // retrieves & displays each user's own collections
    populateCollections('#no_table_coll_acc', lastC, lastS)
   
    // shows corresponding sets whenever different collections are SELECTED
    // it monitors CHANGES on the COLLECTION drop-down selection list
    jQuery('#no_table_coll_acc').change(function(){
        
        var collect = jQuery('#no_table_coll_acc').attr('value');

        // If I choose a collection from the dropdown menu...
        if(collect !== ""){
            // 'reset' Set list
            jQuery('#anthology_set_acc__row').html(selectset);
            // retrieve and populates dropdown with Sets from the Collection
            ajax('show_collection_sets', ['coll_acc'], 'anthology_set_acc');
            
        }
        // NO Collection is selected --> 'resets' protset dropdown to 'zero'
        else{
            jQuery('#anthology_set_acc__row').html(selectset);     // 'resets' Set list
        }

    });

}


// /////////////////////////////////////////////////////////////////////////////////////////////////


// =============================================== //
// Returns the mouse cursor position on the window //
// =============================================== //
function initDivPos(e){
    var posx = 0;
    var posy = 0;
    if (!e) var e = window.event;
    if (e.pageX || e.pageY)     {
        posx = e.pageX;
        posy = e.pageY;
    }
    else if (e.clientX || e.clientY)    {
        posx = e.clientX + document.body.scrollLeft
        + document.documentElement.scrollLeft;
        posy = e.clientY + document.body.scrollTop
        + document.documentElement.scrollTop;
    }

    viewport = jQuery(window).width();
    offset = (posx + 410) - viewport;
    if (offset > 0){ posx = posx - offset;}
    //alert(posx);

    window.posx = posx;
    window.posy = posy;

    //alert(posy);
}



// ================================================= //
// Creates the floating Divs with term info for set  //
// ================================================= //
function createInfo(coll, fset, tid, ont, root){
    
    var divIdName = 'term'+tid+'Div';
    var handleName = 'handle_'+tid;
    var contentName = 'content_'+tid;


    if ( !( document.getElementById(divIdName) ) ){

        // create and position PARENT floater div
        var rootdiv = document.createElement('div');
        rootdiv.id = divIdName;
        rootdiv.className = 'floater';

        rootdiv.style.left = window.posx + 'px'; // h
        rootdiv.style.top = window.posy + 'px';  // v

        // create HANDLE of floater
        var handlediv = document.createElement('div');  
        handlediv.id = handleName;     
        handlediv.className = 'handle';

        // create CONTENT div of floater 
        var contentdiv = document.createElement('div');
        contentdiv.id = contentName;
        contentdiv.className = 'floaterContent';


        document.getElementById('Floaters').appendChild(rootdiv);


        rootdiv.appendChild(contentdiv);
        rootdiv.insertBefore(handlediv, contentdiv);

        // Waiting time spinner...
        msg = '<center>Loading...</center>';
        sid = 'fs'+tid;
        document.getElementById(contentName).innerHTML = loadingMsg(msg, sid); 
        spinner(sid);


	// Get selected evidence codes from explore page...
	var Values = new Array();
	$.each($('input[type="checkbox"]:checked'), function (key, value) {
    	  Values.push($(value).attr("value"));
	});

	var evidence = Values.join();



        ajax('get_handle?coll='+coll+'&fset='+fset+'&tid='+tid+'&root='+root+'&evidence='+evidence, [], handleName);

        ajax('get_floater_content?coll='+coll+'&fset='+fset+'&tid='+tid+'&root='+root+'&ont='+ont+'&evidence='+evidence , [], contentName);

        jQuery('#'+divIdName).draggable( { handle: jQuery('#'+handleName) } );

    }

}


// =============================================== //
//     Saves tabular information as TSV files      // 
// =============================================== //


function saveTSV(coll, fset, ont, tid, type){


if (type == 'terms') {
    var form_name = 't_'+tid
    var file_name = 'terms_'+coll+fset+ont+tid+'.tsv'
}

if (type == 'prots') {
    var form_name = 'f_'+tid
    var file_name = coll+'_'+fset+'_'+ont+tid+'.tsv'
}

var form = document.forms[form_name];

var tsv = '';

var tables = form.getElementsByTagName('table');

if (tables) {

    for (t = 0; t < tables.length; t++) {

      var tbody = tables[t].tBodies.item(0);
      var row = tbody.rows;
        
        for (var j=0; j<row.length; j++) {
          var cell = row[j].cells;
            if (cell[0].firstChild.checked) {
              if (type == 'prots') { tsv += cell[0].firstChild.value + "\t" + cell[3].firstChild.nodeValue + "\n" }
              if (type == 'terms') {
                tsv += cell[1].firstChild.nodeValue +"\t"+ cell[2].firstChild.nodeValue +"\t"+ cell[3].firstChild.nodeValue +"\t"+ cell[4].firstChild.nodeValue +"\t"+ cell[5].firstChild.nodeValue +"\n" }
            }
        }       // closes 'row' iteration  
    }           // closes 'tables' iteration
}    // closes 'tables' IF exists CONDITIONAL



var hidden_list = document.createElement('input');
hidden_list.type = 'hidden';
hidden_list.id = 'content';
hidden_list.name = 'content';
hidden_list.value = tsv;


var save_as = document.createElement('input');
save_as.type = 'hidden';
save_as.id = 'save_as';
save_as.name = 'save_as';
save_as.value = file_name;


form.appendChild(hidden_list);
form.appendChild(save_as);

form.action = 'download_tsv';
form.submit();

form.removeChild(hidden_list);
form.removeChild(save_as);


}


// ======================================= //
// Activates the (horizontal) scrollviewer //
// ======================================= //
function scrollerizer() {

jQuery('#DAGport').scrollview({ grab:"/gryfun/static/images/openhand.cur", grabbing:"/gryfun/static/images/closedhand.cur"});

}


// =========================================================================================== //
// =========================================================================================== //


function loadDAGproper(coll, fset, ont, tid, evid)
{


var aspect = '';
if (ont == 'b'){aspect = 'biological_process';}
if (ont == 'm'){aspect = 'molecular_function';}
if (ont == 'c'){aspect = 'celullar_component';}        

document.getElementById('Floaters').innerHTML = '';

//if (prots == undefined) { prots = 'none'; }

var form_name = 'rr_'+tid;
var form = document.createElement('form');
form.name = form_name;

var coll_id = document.createElement('input');
coll_id.type = 'hidden';
coll_id.id = 'coll';
coll_id.name = 'coll';
coll_id.value = coll;

var fset_id = document.createElement('input');
fset_id.type = 'hidden';
fset_id.id = 'fset';
fset_id.name = 'fset';
fset_id.value = fset;

var ont_n = document.createElement('input');
ont_n.type = 'hidden';
ont_n.id = 'aspect';
ont_n.name = 'aspect';
ont_n.value = aspect;

var newroot = document.createElement('input');
newroot.type = 'hidden';
newroot.id = 'newroot';
newroot.name = 'newroot';
newroot.value = tid;

var evidence = document.createElement('input');
evidence.type = 'hidden';
evidence.id = 'evidence';
evidence.name = 'evidence';
evidence.value = evid;

form.appendChild(coll_id);
form.appendChild(fset_id);
form.appendChild(ont_n);
form.appendChild(newroot);
form.appendChild(evidence);


// Waiting time spinner...
msg = '<center>Please wait...</center>';
sid = 'rrs'+tid;
document.getElementById('centralDiv').innerHTML = loadingMsg(msg, sid); 
spinner(sid);


// create and position PARENT floater div
var Floaters = document.createElement('div');
Floaters.id = 'Floaters';
Floaters.style.position = 'absolute';
Floaters.style.left = '0px';
Floaters.style.top = '0px';

document.body.appendChild(Floaters);


jQuery.ajax({
        url: "displayDAG",
        data: { coll: coll, fset: fset, aspect: aspect, newroot: tid, evidence: evid },
        type: "POST",
        //dataType: "html",
        success: function(data) {
                    jQuery('#centralDiv').html(data);
        },
        error: function (xhr, status) {
            alert("Sorry, there was a problem!");
        },
        complete: function (xhr, status) {
            scrollerizer();
        }
    });


} // closes loadDAGproper()





// =========================================================================================== //

// FOR THE FLOATING DIVS CHECKBOXES

// nice and simple check/uncheck group of checkboxes
// gotten somewhere and tuned in to adapt to the
// single checkbox issue.
function checkUncheckAll(checkAllState, cbGroup)
{
    if (typeof cbGroup.length != 'undefined') {
      for (i = 0; i < cbGroup.length; i++)
      {
        cbGroup[i].checked = checkAllState.checked;
      }
    } else {  
      cbGroup.checked = checkAllState.checked;
    }
}


function checkUncheckAllToo(theElement) {
  var theForm = theElement.form, z = 0;
  for(z=0; z<theForm.length;z++){
    if(theForm[z].type == 'checkbox' && theForm[z].name != 'checkall'){
      theForm[z].checked = theElement.checked;
    }
  }
}


// ============================================================================================ //

// Load message + spinner for AJAX requests
function loadingMsg(msg, sid) {

var rsp = ' <div class="loadmsg">'+msg+'</div>      \
<br/>                                                   \
<div id="'+sid+'" class="spinnerdiv">                    \
<div class="bar1"></div>                                \
<div class="bar2"></div>                                \
<div class="bar3"></div>                                \
<div class="bar4"></div>                                \
<div class="bar5"></div>                                \
<div class="bar6"></div>                                \
<div class="bar7"></div>                                \
<div class="bar8"></div>                                \
</div>';

return rsp
}

// Loading spinner! Just provide a _target_ div, 
// include in the html, summon and watch it spin!
function spinner(target) {

var count = 0;

  function rotate() {
    var elem2 = document.getElementById(target);
    try { elem2.style.MozTransform = 'scale(0.5) rotate('+count+'deg)'; } catch(err){ }
    try { elem2.style.WebkitTransform = 'scale(0.5) rotate('+count+'deg)'; } catch(err){ }
    if (count==360) { count = 0 }
    count+=45;
    window.setTimeout(rotate, 100);
  }
  window.setTimeout(rotate, 100);
}
